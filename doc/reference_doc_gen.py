#!/usr/bin/python3

from __future__ import annotations
import sys
import os

import gi

gi.require_version('Gdk', '4.0')
gi.require_version('Gtk', '4.0')
gi.require_version('Rsvg', '2.0')

doc_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.dirname(doc_dir)
plugins_dir = os.path.join(project_dir, 'turtlico', 'plugins')

sys.path.insert(0, project_dir)

import turtlico.lib as lib

# ------------ Rendering images ------------
"""
import cairo
from gi.repository import Gtk, Gio
images_in = os.path.join(doc_dir, 'figures.in')
images_out = os.path.join(doc_dir, 'figures')

widget = Gtk.Label.new()

for imgfile in os.listdir(images_in):
    if not imgfile.endswith('.tcp'):
        continue
    abspath = os.path.join(images_in, imgfile)
    name, ext = os.path.splitext(imgfile)
    output = os.path.join(images_out, name + '.svg')

    buffer = lib.ProjectBuffer()
    buffer.load_from_file(Gio.File.new_for_path(abspath))

    snapshot = Gtk.Snapshot.new()
    width, height = lib.icon.append_block_to_snapshot(
        buffer.code.lines, snapshot, 0, 0,
        widget, lib.icon.get_default_colors(), buffer.code)

    node = snapshot.to_node()  #FIXME: Not working due to a bug in Gtk 4
    with cairo.SVGSurface(output, width, height) as surface:
        cr = cairo.Context(surface)
        node.draw(cr) """

# ------------ Docs files ------------
INDENTATION = ' ' * 3


def load_commands_from_docs(file: str) -> dict[str, str]:
    if not os.path.isfile(file):
        return {}

    with open(file) as f:
        doc_file = f.readlines()

    old_commands = {}

    current_command = None
    current_command_id = None
    # 0 - searching for container, 1 - needs ID
    # 2 needs blank line, 3 - content
    phase = 0

    def save_command(current_command):
        if phase != 3:
            return current_command
        if current_command_id is not None:
            old_commands[current_command_id] = current_command
            return None
        return current_command
    for i, line in enumerate(doc_file):
        if phase == 3 and line.startswith('..'):
            current_command = save_command(current_command)
            phase = 0

        if phase == 0 and line.strip() == '.. container:: cmd':
            phase = 1
            continue
        if phase == 1:
            if line.startswith(f'{INDENTATION}:name: '):
                current_command_id = line[10:].strip()
                phase = 2
                continue
            print(f'Invalid file. Command in line {i + 1} has no id.')
            sys.exit(0)
        if phase == 2:
            if line.strip() == '':
                current_command = []
                phase = 3
                continue
            print(f'Invalid file. No blank line at line {i + 1}')
            sys.exit(0)

        if phase == 3:
            current_command.append(
                line.replace(INDENTATION, '', 1).replace('\n', ''))
    current_command = save_command(current_command)
    return old_commands


# Load definition files
plugins = lib.Plugin.get_from_paths(lib.Plugin.get_paths_in_path(plugins_dir))

new_commands = {}
for plugin_id, plugin in plugins.items():
    doc_file_path = os.path.join(doc_dir, f'ref_{plugin_id}.rst')

    old_commands = load_commands_from_docs(doc_file_path)
    # Get commands from the definition file
    new_commands = {}
    for category in plugin.categories:
        for command in category.command_definitions:
            id = lib.CommandDefinition.escape_id_for_html(command.id)
            if id in old_commands.keys():
                new_commands[id] = old_commands[id]
                del old_commands[id]
                continue
            new_commands[id] = [
                f'**{command.help}**',
                '']
            if command.command_type == lib.CommandType.METHOD:
                new_commands[id].extend(['*Parameters*', '', '* None'])

    for c in old_commands:
        print("Warning: Command '{}' was not found in commmand definitions of plugin '{}' so it was removed from docs.".format(c.replace('\n', ''), plugin_id))  # noqa: E501

    output_file = []
    title = f"{plugin.name} docs"
    overline = len(title) * '='
    output_file.append(f"""{overline}\n{title}\n{overline}\n""")

    for id, content in new_commands.items():
        content = '\n'.join(
            [(INDENTATION + line if line.strip() else '') for line in content])
        output_file.append(
            f""".. container:: cmd
{INDENTATION}:name: {id}

{content}
"""
        )

    with open(doc_file_path, 'w') as f:
        for c in output_file:
            f.write(c)


with open(os.path.join(doc_dir, 'reference_doc.rst'), 'w') as f:
    f.write(f"""=======================
Reference documentation
=======================

.. toctree::
{INDENTATION}:maxdepth: 2
{INDENTATION}:titlesonly:

""")
    for pid in plugins.keys():
        f.write(f'{INDENTATION}ref_{pid}\n')
