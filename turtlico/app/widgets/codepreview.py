# Copyright (C) 2021 saytamkenorh
#
# This file is part of Turtlico.
#
# Turtlico is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turtlico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Turtlico.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from gi.repository import Gtk, GObject

import turtlico.app.widgets as widgets
from turtlico.locale import _


@Gtk.Template(resource_path='/io/gitlab/Turtlico/ui/codepreview.ui')
class CodePreview(Gtk.TextView):
    __gtype_name__ = 'TurtlicoCodePreview'

    _programview: widgets.ProgramView

    def __init__(self) -> None:
        super().__init__()
        self._project_buffer = None
        self._programview = None

    def set_programview(self, programview: widgets.ProgramView):
        assert isinstance(programview, widgets.ProgramView)

        if self._programview is not None:
            raise Exception('Programview is already set')
        self._programview = programview
        self._programview.connect('preview-update', self._on_preview_update)
        self.update()

    def scroll_to(self, x: int, y: int):
        """Scrolls to icon at x, y

        Args:
            x (int): Icon x
            y (int): Icon line
        """
        if not self.get_mapped():
            return
        dinfo = self._programview.preview_debug_info
        if dinfo is None:
            return

        while (line := dinfo.get((x, y), None)) is None and x > 0:
            x -= 1
        if line is None:
            return
        line -= 1

        line_iter_ok, line_iter = self.props.buffer.get_iter_at_line(line)
        if not line_iter_ok:
            return
        line_iter_end = line_iter.copy()
        if line_iter_end is None:
            return
        line_iter_end.forward_to_line_end()

        self.scroll_to_iter(line_iter, 0.25, False, 0, 0)
        self.props.buffer.select_range(line_iter, line_iter_end)

    def unselect(self):
        start_mark = self.props.buffer.get_selection_bound()
        start = self.props.buffer.get_iter_at_mark(start_mark)
        self.props.buffer.select_range(start, start)

    def update(self):
        assert self._programview is not None
        if not self.get_mapped():
            return
        vval = self.props.vadjustment.props.value
        code = self._programview.preview_code

        self.props.buffer.props.text = (
            code if code else _('Code preview unavailable'))
        self.props.vadjustment.props.value = vval + 0.00000000001

    def _on_preview_update(self, widget):
        self.update()

    @Gtk.Template.Callback()
    def _on_map(self, widget):
        self.update()


GObject.type_register(CodePreview)
