# utils.py
#
# Copyright 2020 saytamkenorh
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

import os
import sys
import inspect
from datetime import datetime
from typing import Callable, Union

from gi.repository import GLib, Gio, Gdk, Gtk

from turtlico.locale import _


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def rgba(string):
    c = Gdk.RGBA()
    c.parse(string)
    return c


def _get_time() -> str:
    now = datetime.now()
    return now.strftime('%H:%M:%S')


def _debug_message(mtype, color, msg, output):
    s = inspect.stack()[2]
    filename = os.path.basename(s.filename)
    print(
        f'[Turtlico] {color}{bcolors.BOLD}{mtype}{bcolors.ENDC} {filename}:{s.lineno} ({_get_time()}): {msg}',  # NOQA
        file=output)


def debug(msg):
    _debug_message('DEBUG', bcolors.OKGREEN, msg, sys.stdout)


def error(msg):
    _debug_message('ERROR', bcolors.FAIL, msg, sys.stderr)


def msg(msg):
    _debug_message('INFO', bcolors.OKBLUE, msg, sys.stdout)


def warn(msg):
    _debug_message('WARNING', bcolors.WARNING, msg, sys.stderr)


def new_shortcut(trigger: str,
                 callback: Callable[[Gtk.Widget, GLib.Variant], bool]
                 ) -> Gtk.Shortcut:
    _trigger = Gtk.ShortcutTrigger.parse_string(trigger)
    _action = Gtk.CallbackAction.new(callback)
    shortcut = Gtk.Shortcut.new(_trigger, _action)
    return shortcut


def new_action_shortcut(trigger: str, action: str) -> Gtk.Shortcut:
    _trigger = Gtk.ShortcutTrigger.parse_string(trigger)
    _action = Gtk.NamedAction.new(action)
    shortcut = Gtk.Shortcut.new(_trigger, _action)
    return shortcut


def win_add_action(win: Gtk.Window,
                   name: str, shortcut: str,
                   callback: Callable[[Gio.SimpleAction, GLib.Variant], None]
                   ) -> Gio.SimpleAction:
    action = Gio.SimpleAction.new(name, None)
    win.add_shortcut(
        new_action_shortcut(shortcut, f'win.{name}'))
    action.connect('activate', callback)
    win.add_action(action)
    return action


def group_add_action(group: Gio.ActionGroup,
                     name: str,
                     callback: Callable[[Gio.SimpleAction, GLib.Variant]]
                     ) -> Gio.SimpleAction:
    action = Gio.SimpleAction.new(name, None)
    action.connect('activate', callback)
    group.add_action(action)
    return action


def filedialog(parent: Gtk.Window, title: str, action: Gtk.FileChooserAction,
               types: list[tuple[str, str, str]],
               callback: Callable[[Union[Gio.File, None]], None]):
    """Opens a file dialog and calls callback.

    Args:
        parent (Gtk.Window): Parent window for the dialog
        title (str): Title for the dialog
        action (Gtk.FileChooserAction): Dialog action
        types (list[tuple[str, str, str]]):
            List of filter tuples (name, extension, mime)
        callback (Callable[[Union[str, None]], None]):
            The callback. Receives the path or None.
    """
    filters = []
    for t in types:
        filefilter = Gtk.FileFilter.new()
        filefilter.props.name = f'{t[0]} ({t[1]})'
        filefilter.add_pattern(f'*{t[1]}')
        filefilter.add_mime_type(t[2])
        filters.append(filefilter)

    accept_label = (
        _('Save') if action == Gtk.FileChooserAction.SAVE else _('Open'))
    cancel_label = _('Cancel')

    def response(dialog, resp):
        f = dialog.get_file()
        dialog.destroy()

        if resp == Gtk.ResponseType.CANCEL or f is None:
            callback(None)
            return

        if not os.path.splitext(f.get_basename())[1]:
            if len(types) > 0:
                f = Gio.File.new_for_path(
                    f.get_path() + types[0][1]
                )
        callback(f)

    platform = sys.platform
    if platform == 'linux':
        # Flatpak portal can't be used because it may not return
        # the actual path of the file.
        # This would cause that the program would not be able to read
        # ./* files
        dialog = Gtk.FileChooserDialog(
            title=title, transient_for=parent, action=action, modal=True)
        dialog.add_buttons(
            accept_label, Gtk.ResponseType.ACCEPT,
            cancel_label, Gtk.ResponseType.CANCEL)
        dialog.set_default_response(Gtk.ResponseType.ACCEPT)
        for f in filters:
            dialog.add_filter(f)
        dialog.show()
        dialog.connect('response', response)
        return

    dialog = Gtk.FileChooserNative(
        title=title, transient_for=parent, action=action, modal=True,
        accept_label=accept_label, cancel_label=cancel_label)
    # FIXME: causes crash on Windows
    if sys.platform != 'win32':
        for f in filters:
            dialog.add_filter(f)
    dialog.show()
    dialog.connect('response', response)


def get_parent_window(widget: Gtk.Widget) -> Union[Gtk.Window, None]:
    w = None
    p = widget
    while p := p.get_parent():
        w = p
    if w == widget or not isinstance(w, Gtk.Window):
        return None
    return w
