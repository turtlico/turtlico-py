============================
Gpiozero (Raspberry Pi) docs
============================
.. container:: cmd
   :name: rpi-led

   **Connect LED**

   Returns an object to control the part.
   You can use `on <#rpi-on>`_ and `off <#rpi-off>`_ commands to control it.
   To get or set current state use the `value <#rpi-value>`_ property.
   To determine which pin is the part connected into you can see `the pin numbering scheme <https://gpiozero.readthedocs.io/en/stable/recipes.html#pin-numbering_>`.

   *Parameters*

   * ``NUMBER`` Pin where the LED is physically connected
.. container:: cmd
   :name: rpi-rgb

   **Connect RGB LED**

   Returns an object to control the part. Use `value <#rpi-value>`_ and `RPi color <#rpi-color>`_ to control the color of the LED.

   *Parameters*

   * ``NUMBER`` "red" Pin that controls the red component of the RGB LED
   * ``NUMBER`` "green" Pin that controls the green component of the RGB LED
   * ``NUMBER`` "blue" Pin that controls the blue component of the RGB LED

.. container:: cmd
   :name: rpi-btn

   **Connect Button**

   Returns an object check state of the part. The part can also be any digital input device.

   *Parameters*

   * ``NUMBER`` Pin where the Button or other digital input device is physically connected.
.. container:: cmd
   :name: rpi-pwm

   **Connect PWM Output Device**

   Returns an object to control a PWM output device.
   PWM means that the `value <#rpi-value>`_ of the device can be anywhere in range from 0 to 1 (including decimal values).
   The signal is then quickly switched on and off depending on the value. 0.0 is off and 1.0 is fully on.

   *Parameters*

   * ``NUMBER`` Pin where the part is connected.
.. container:: cmd
   :name: rpi-out

   **Connect Digital Output Device**

   Retruns an object to control a device that has two states: on and off
   You can use `on <#rpi-on>`_ and `off <#rpi-off>`_ commands to control it
   To get or set current state use the `value <#rpi-value>`_ property

   *Parameters*

   * ``NUMBER`` Pin where the part is connected.
.. container:: cmd
   :name: rpi-on

   **Turn on device method**

   *Parameters*

   * None
.. container:: cmd
   :name: rpi-off

   **Turn off device method**

   *Parameters*

   * None
.. container:: cmd
   :name: rpi-value

   **Value property (input and output)**

   You can get or set the state of a device via this property. See individual devices for details.

.. container:: cmd
   :name: rpi-color

   **Converts color to format suitable for use with gpiozero**

   Gpiozero functions (i.e. value of LEDs) do not accept the normal colors represented by three values in range 0 - 255.
   This functions converts the normal Turtlico color to the format accepted by gpiozero (three values in range 0.0 - 1.0).

   *Parameters*

   * ``COLOR`` The color to convert
.. container:: cmd
   :name: rpi-event-on

   **Connect a function to check when device is turned on**

   *Parameters*

   * ``DEVICE`` The device.
   * ``FUNCTION`` Callback with no arguments
.. container:: cmd
   :name: rpi-event-off

   **Connect a function to check when device is turned off**

   *Parameters*

   * ``DEVICE`` The device.
   * ``FUNCTION`` Callback with no arguments
