# Copyright (C) 2021 saytamkenorh
#
# This file is part of Turtlico.
#
# Turtlico is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turtlico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Turtlico.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import tempfile
import subprocess
import threading
import signal
from typing import Union, Tuple

from gi.repository import GObject, GLib

import turtlico.lib as lib
import turtlico.utils as utils
from turtlico.locale import _

_launcher = """
import sys
errfile = sys.stderr
if '{1}' == 'True':
    errfile = open(r'{2}', 'w')
try:
    exec(open(r'{0}', 'r', encoding='utf-8').read(), {{'__file__':r'{0}'}})
except SyntaxError as e:
    print(f'TURTLICO_TRACE:{{e.lineno}}', file=errfile)
    print('Syntax error', file=errfile)
    if '{1}' == 'True':
        import os, signal
        errfile.close()
        os.kill(os.getppid(), signal.SIGTERM)
except Exception as e:
    import traceback, turtle
    exception_type, exception_object, exception_traceback = sys.exc_info()
    trace = traceback.extract_tb(exception_traceback)
    line_numbers = ','.join(reversed([str(t[1]) for t in trace]))
    message = str(e).strip()
    if isinstance(e, turtle.Terminator) or 'invalid command name' in message:
        print('TURTLICO_TRACE:IGNORE', file=errfile)
    print(f'TURTLICO_TRACE:{{line_numbers}}', file=errfile)
    if message != '':
        print(message, file=errfile)
        if '{1}' == 'True':
            import os, signal
            errfile.close()
            os.kill(os.getppid(), signal.SIGTERM)
"""

_idle_exit = """
except SystemExit:
    print('-' * 20)
    input(r'{0}')
    import os, signal, io
    if isinstance(errfile, io.IOBase):
        errfile.close()
    os.kill(os.getppid(), signal.SIGTERM)
print('-' * 20)
input(r'{0}')
import os, signal, io
if isinstance(errfile, io.IOBase):
    errfile.close()
os.kill(os.getppid(), signal.SIGTERM)
"""


class DebuggingReuslt(GObject.Object):
    program_failed = GObject.Property(type=bool, default=False)
    error_message = GObject.Property(type=str)

    def __init__(self, debug_info: lib.DebugInfo, stderr: str):
        super().__init__()
        if stderr:
            self.props.program_failed = True
            debug_info = dict(sorted(debug_info.items()))
            stderr_lines = stderr.splitlines()
            utils.debug(f'Program stderr:\n{stderr}')

            coord = None
            # Get program line
            trace_prefix = 'TURTLICO_TRACE:'
            for line in stderr_lines:
                if line.startswith(trace_prefix):
                    trace = line[len(trace_prefix):].split(',')
                    if 'IGNORE' in trace:
                        self.props.program_failed = False
                    # Remove TURTLICO_TRACE from error message
                    stderr_lines = stderr_lines[1:]
                    for t in trace:
                        try:
                            python_line = int(t)
                        except Exception:
                            pass
                        else:
                            t_coord = self._python_to_icons_coord(
                                debug_info, python_line)
                            if t_coord is not None:
                                coord = t_coord
                                break
                    break

            if coord is None:
                coord_msg = _('Error occured outside of your program.')
            else:
                coord_msg = _('Error occured on line {} column {}.').format(
                    coord[1] + 1, coord[0] + 1)

            self.error_message = '{}\n{}'.format(
                '\n'.join(stderr_lines), coord_msg)
        else:
            self.program_failed = False

    def _python_to_icons_coord(self,
                               debug_info: lib.DebugInfo,
                               line: int) -> Union[Tuple[int, int], None]:
        coord = debug_info.get(line, None)
        if coord is not None:
            return coord
        for k in reversed(debug_info.keys()):
            if k < line:
                return debug_info[k]
        return None


def _get_python() -> str:
    platform = sys.platform
    if platform == 'win32':
        bindir = os.path.dirname(os.path.abspath(sys.argv[0]))
        python = os.path.join(bindir, 'pythonw.exe')
        return python
    return sys.executable


class Debugger(GObject.Object):
    debug_info: lib.DebugInfo
    tempdir: tempfile.TemporaryDirectory  # Used for unsaved files
    path: str  # Path of the compiled file
    stderr_path: str  # Path to stderr file (when using idle)

    subprocpid: int  # PID of the process
    subprocpid_lock: threading.RLock

    @GObject.Property
    def running(self) -> bool:
        self.subprocpid_lock.acquire()
        running = self.subprocpid is not None
        self.subprocpid_lock.release()
        return running
    use_idle = GObject.Property(default=False, type=bool)

    @GObject.Signal(flags=GObject.SignalFlags.RUN_LAST,
                    arg_types=(DebuggingReuslt,))
    def debugging_done(self, *args):
        pass

    def __init__(self,
                 project: lib.ProjectBuffer,
                 compiler: lib.Compiler, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.tempdir = None
        self.subprocpid = None
        self.subprocpid_lock = threading.RLock()

        code, debug_info = compiler.compile(project.code.lines)
        self.debug_info = debug_info
        utils.debug('Generated code:')
        utils.debug(code)

        self.path = None
        if project._project_file:
            self.path = project._project_file.get_path() + '.py'
        else:
            self.tempdir = tempfile.TemporaryDirectory(prefix='turtlico_')
            self.path = os.path.join(self.tempdir.name, 'program.py')

        assert isinstance(self.path, str)
        with open(self.path, 'w', encoding='utf-8') as f:
            f.write(code)

        self.props.use_idle = project.props.run_in_console

    def dispose(self):
        if self.props.running:
            self.stop()
        if self.tempdir:
            try:
                self.tempdir.cleanup()
            except Exception as e:
                utils.warn(f'Cannot delete run temp folder: {e}')

    def run(self):
        if self.props.running:
            return
        # Sets something to subprocpid in order to prevent
        # from starting two threads at once
        self.subprocpid_lock.acquire()
        self.subprocpid = -1
        self.subprocpid_lock.release()

        self.props.running = True
        # The child program is run as a separate process due to safety reasons
        if self.props.use_idle:
            if self.tempdir is None:
                self.tempdir = tempfile.TemporaryDirectory(prefix='turtlico_')
            self.stderr_path = os.path.join(self.tempdir.name, 'stderr.txt')
        else:
            self.stderr_path = ''

        launcher = _launcher.format(
            self.path, str(self.props.use_idle), self.stderr_path)
        if self.props.use_idle:
            launcher += _idle_exit.format(
                _('Press enter to close this window'))

        args = [_get_python()]
        if self.props.use_idle:
            args.extend(['-m', 'idlelib', '-t', 'Turtlico'])
        args.extend(['-c', launcher])
        thread = threading.Thread(
            target=self._run_child, args=[args], daemon=True)
        thread.start()

    def stop(self):
        self.subprocpid_lock.acquire()
        assert self.props.running is True

        platform = sys.platform
        try:
            if platform == 'win32':
                os.kill(self.subprocpid, 2)
            else:
                os.kill(self.subprocpid, signal.SIGKILL)
        except Exception as e:
            utils.error(f'Cannot stop debugging: "{e}"')
        self.subprocpid_lock.release()

    def _run_child(self, args):
        self.subprocpid_lock.acquire()

        subproc = subprocess.Popen(
            args,
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.subprocpid = subproc.pid

        self.subprocpid_lock.release()

        stdout, stderr = subproc.communicate()
        GLib.idle_add(self._run_child_done, stderr.decode('utf-8'))

    def _run_child_done(self, stderr: str) -> bool:
        self.subprocpid_lock.acquire()
        self.subprocpid = None
        self.subprocpid_lock.release()

        if self.props.use_idle:
            if os.path.isfile(self.stderr_path):
                with open(self.stderr_path, 'r') as f:
                    stderr = f.read()

        result = DebuggingReuslt(self.debug_info, stderr)

        self.emit('debugging-done', result)
        return GLib.SOURCE_REMOVE
