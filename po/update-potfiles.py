#!/usr/bin/env python3
# Copyright (C) 2021 saytamkenorh
#
# This file is part of Turtlico.
#
# Turtlico is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turtlico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Turtlico.  If not, see <http://www.gnu.org/licenses/>.

import os

potfiles = []

projdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

walk = os.walk(os.path.join(projdir, 'turtlico'))
for root, dirs, files in walk:
    for name in files:
        if name.endswith('.py'):
            abspath = os.path.join(root, name)
            content = open(abspath, 'r').read()
            if 'from turtlico.locale import _' in content:
                potfiles.append(os.path.relpath(abspath, projdir))

walk = os.walk(os.path.join(projdir, 'data/ui'))
for root, dirs, files in walk:
    for name in files:
        if name.endswith('.ui'):
            abspath = os.path.join(root, name)
            potfiles.append(os.path.relpath(abspath, projdir))

potfiles.extend([
    'data/io.gitlab.Turtlico.appdata.xml.in',
    'data/io.gitlab.Turtlico.desktop.in',
    'data/io.gitlab.Turtlico.gschema.xml',
])

potfiles.sort()

with open(os.path.join(projdir, 'po/POTFILES'), 'w') as f:
    f.write('\n'.join(potfiles))
