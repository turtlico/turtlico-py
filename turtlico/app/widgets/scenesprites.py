# Copyright (C) 2021 saytamkenorh
#
# This file is part of Turtlico.
#
# Turtlico is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turtlico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Turtlico.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

import os
import io
import urllib.request

from typing import Union
from PIL import Image
from gi.repository import GObject, GLib, Gio, Gtk, Gdk, GdkPixbuf

import turtlico.app.widgets as widgets
import turtlico.lib as lib
import turtlico.utils as utils
from turtlico.locale import _


@Gtk.Template(resource_path='/io/gitlab/Turtlico/ui/spritewidget.ui')
class SpriteWidget(Gtk.Box):
    __gtype_name__ = 'TurtlicoSpriteWidget'

    _picture: Gtk.Picture = Gtk.Template.Child()
    _label: Gtk.Label = Gtk.Template.Child()
    _drag_source: Gtk.DragSource
    _scenebuffer: lib.SceneBuffer
    _sprite_texture_binding: GObject.Binding
    _sprite_ext: str

    popover: Gtk.Popover
    entry: widgets.ItemEntry

    sprite: lib.SpriteInfo
    colors: lib.icon.CommandColorScheme

    def __init__(self,
                 scenebuffer: lib.SceneBuffer,
                 colors: lib.icon.CommandColorScheme):
        super().__init__()
        self.sprite = None
        self._sprite_texture_binding = None
        self._scenebuffer = scenebuffer
        self.colors = colors
        self._sprite_ext = ''

        self.popover = Gtk.Popover.new()
        self.popover.props.autohide = True

        self.entry = widgets.ItemEntry('')
        self.entry.connect('delete', self._on_edit_delete)
        self.entry.connect('rename', self._on_edit_rename)
        self.entry.connect('edit-cancel', self._on_edit_cancel)
        self.popover.props.child = self.entry

        self.append(self.popover)

        self._drag_source = Gtk.DragSource.new()
        self._drag_source.set_actions(Gdk.DragAction.COPY)
        self._drag_source.connect('prepare', self._on_drag_prepare)
        self.add_controller(self._drag_source)

    def set_sprite(self, sprite: lib.SpriteInfo):
        if self._sprite_texture_binding is not None:
            self._sprite_texture_binding.unbind()

        self.sprite = sprite
        self.props.visible = sprite is not None
        if sprite is None:
            return
        self._sprite_texture_binding = self.sprite.bind_property(
            'texture', self._picture, 'paintable',
            GObject.BindingFlags.SYNC_CREATE)
        self._label.props.label = sprite.name

    @Gtk.Template.Callback()
    def _on_button_clicked(self, btn: Gtk.Button):
        if self.sprite.texture_file is None:
            return
        self.entry.original_name, self._sprite_ext = os.path.splitext(
            self.sprite.name)

        self.entry.edit()
        self.popover.popup()

    def _on_edit_delete(self, obj):
        self.popover.popdown()

        def __delete(dialog, response: Gtk.ResponseType):
            dialog.close()
            if response != Gtk.ResponseType.YES:
                return
            try:
                self.sprite.texture_file.delete()
            except GLib.Error as e:
                utils.error(f'Cannot delete the sprite: {e.message}')
        dialog = Gtk.MessageDialog(
            modal=True,
            transient_for=utils.get_parent_window(self),
            text=_('Do you really want to permanently delete the sprite "{}"?'
                   ).format(self.sprite.name),
            secondary_text=_(
                'You should also remove the sprite from all the scenes'
                ' that contains it.'
                ' Removed sprites are displayed as a red square.'),
            message_type=Gtk.MessageType.QUESTION)
        dialog.add_buttons(
            _('Delete'), Gtk.ResponseType.YES,
            _('Cancel'), Gtk.ResponseType.CANCEL)
        delete_btn = dialog.get_widget_for_response(Gtk.ResponseType.YES)
        delete_btn.get_style_context().add_class('destructive-action')

        dialog.connect('response', __delete)
        dialog.show()

    def _on_edit_rename(self, obj, new_name):
        self.popover.popdown()

        new_name = new_name + self._sprite_ext
        old_name = self.sprite.name
        if new_name == self.sprite.texture_file.get_basename():
            return

        try:
            self.sprite.texture_file.set_display_name(new_name)
            self._scenebuffer.replace_sprite_name(old_name, new_name)
        except GLib.Error as e:
            dialog = Gtk.MessageDialog(
                modal=True,
                text=_('Cannot rename the sprite: "{}"').format(e.message),
                transient_for=utils.get_parent_window(self),
                message_type=Gtk.MessageType.ERROR)

            def _close(dialog, response):
                dialog.close()

            dialog.connect('response', _close)
            dialog.show()

    def _on_edit_cancel(self, obj):
        self.popover.popdown()

    def _on_drag_prepare(self,
                         source: Gtk.DragSource, x: float, y: float
                         ) -> Gdk.ContentProvider:
        cmd, ok = self._scenebuffer.project.get_command(
            'img', self.sprite.name)
        if not ok:
            return None
        commands = [[cmd]]
        return lib.icon.prepare_drag(
            source, commands, self, self.colors,
            self._scenebuffer.project.code)


@Gtk.Template(resource_path='/io/gitlab/Turtlico/ui/scenesprites.ui')
class SceneSprites(Gtk.Box):
    __gtype_name__ = 'TurtlicoSceneSprites'

    _sprites_view: Gtk.GridView = Gtk.Template.Child()
    _sprites_view_factory: Gtk.SignalListItemFactory
    _drop_target: Gtk.DropTarget

    _sprites_view_model: Gtk.NoSelection
    _scenebuffer: lib.SceneBuffer
    _sprites: Gio.ListStore

    colors: lib.icon.CommandColorScheme

    def __init__(self):
        super().__init__()
        self._scenebuffer = None

        self._sprites = Gio.ListStore.new(lib.SpriteInfo)
        self._sprites_view_model = Gtk.NoSelection.new(self._sprites)

        self._sprites_view_factory = Gtk.SignalListItemFactory.new()
        self._sprites_view_factory.connect('bind', self._sprite_bind)
        self._sprites_view_factory.connect('setup', self._sprite_setup)
        self._sprites_view_factory.connect('teardown', self._sprite_teardown)
        self._sprites_view_factory.connect('unbind', self._sprite_unbind)
        self._sprites_view.props.factory = self._sprites_view_factory

        self._sprites_view.set_model(self._sprites_view_model)

        self._drop_target = Gtk.DropTarget.new(
            GObject.Object, Gdk.DragAction.COPY | Gdk.DragAction.MOVE)
        self._drop_target.set_gtypes([GdkPixbuf.Pixbuf, Gio.File, str])
        self._drop_target.connect('drop', self._on_drop_target_drop)
        self._sprites_view.add_controller(self._drop_target)

    def set_scenebuffer(self, buffer: lib.SceneBuffer):
        if self._scenebuffer is not None:
            raise Exception('Scene buffer is already set')

        self._scenebuffer = buffer
        self._scenebuffer.connect(
            'project_scan', self._on_scenebuffer_project_scan)
        self.reload_sprites()

    def reload_sprites(self):
        self._sprites.remove_all()
        for name, info in self._scenebuffer.sprites.items():
            self._sprites.append(info)

    def _on_scenebuffer_project_scan(self, *args):
        self.reload_sprites()

    def _sprite_bind(self, factory, item: Gtk.ListItem):
        sprite: lib.SpriteInfo = item.props.item
        item.props.child.set_sprite(sprite)

    def _sprite_setup(self, factory, item: Gtk.ListItem):
        widget = SpriteWidget(self._scenebuffer, self.colors)
        item.props.child = widget

    def _sprite_teardown(self, factory, item: Gtk.ListItem):
        pass

    def _sprite_unbind(self, factory, item: Gtk.ListItem):
        item.props.child.set_sprite(None)

    def _on_drop_target_drop(self,
                             dt: Gtk.DropTarget,
                             drop: Union[Gio.File, GdkPixbuf.Pixbuf, str], x, y
                             ):
        if isinstance(drop, GdkPixbuf.Pixbuf):
            img = Image.frombuffer(
                'RGBA', (drop.get_width(), drop.get_height()),
                drop.get_pixels(), 'raw', 'RGBA', 0, 1)
            img.save(self._get_filename(_('Image'), '.gif'), 'GIF')
            return True
        elif isinstance(drop, Gio.File):
            return self._load_from_gfile(drop)
        elif isinstance(drop, str):
            try:
                with urllib.request.urlopen(drop) as f:
                    return self._load_from_stream(f, drop)
            except ValueError:
                return False
            except OSError as e:
                utils.warn(f'Cannot import image: {e}')
        return False

    def _load_from_gfile(self, file: Gio.File):
        try:
            ins = file.read()
            drop_data = bytes()
            while (buf := ins.read_bytes(8192)).get_size() > 0:
                drop_data += buf.get_data()
        except GLib.Error as e:
            utils.warn(f'Cannot import image: {e.message}')
            return False
        return self._load_from_stream(
            io.BytesIO(drop_data), file.get_basename())

    def _load_from_stream(self, stream, path):
        try:
            img = Image.open(stream)
        except OSError as e:
            utils.warn(f'Cannot process imported image: {e}')
            return False

        basename = os.path.splitext(os.path.basename(path))[0]
        targetpath = self._get_filename(basename, '.gif')
        img.save(targetpath, 'GIF')

        return True

    def _get_filename(self, basename, extension):
        pf: Gio.File = self._scenebuffer.project.props.project_file
        pd: Gio.File = pf.get_parent()
        child = pd.get_child(basename + extension)
        number = 2

        while child.query_exists():
            child = pd.get_child(f'{basename} ({number}){extension}')
            number += 1

        return child.get_path()


GObject.type_register(SceneSprites)
