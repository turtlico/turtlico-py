# Copyright (C) 2021 saytamkenorh
#
# This file is part of Turtlico.
#
# Turtlico is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turtlico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Turtlico.  If not, see <http://www.gnu.org/licenses/>.

import os
from typing import Callable

from gi.repository import GObject, GLib, Gio, Gtk

import turtlico.lib as lib
import turtlico.lib.icon as icon
import turtlico.utils as utils

from turtlico.app.debugger import Debugger, DebuggingReuslt
from turtlico.app.updater import Updater
import turtlico.app.widgets as widgets
from turtlico.locale import _


@Gtk.Template(resource_path='/io/gitlab/Turtlico/ui/mainwindow.ui')
class MainWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'TurtlicoMainWindow'

    _toolbar_box: Gtk.Box = Gtk.Template.Child()
    _toolbar_left_box: Gtk.Box = Gtk.Template.Child()
    _toolbar_right_box: Gtk.Box = Gtk.Template.Child()

    _icon_view: widgets.IconsView = Gtk.Template.Child()
    _program_view: widgets.ProgramView = Gtk.Template.Child()
    _status_bar: Gtk.Label = Gtk.Template.Child()
    _program_view_search: widgets.ProgramViewSearch = Gtk.Template.Child()
    _program_view_search_rev: Gtk.Revealer = Gtk.Template.Child()
    notifications: widgets.AppNotifications = Gtk.Template.Child()
    _codepreview: widgets.CodePreview = Gtk.Template.Child()
    _codepreview_sw: Gtk.ScrolledWindow = Gtk.Template.Child()

    _run_btn = Gtk.Template.Child()
    _run_btn_img = Gtk.Template.Child()
    _undo_btn = Gtk.Template.Child()
    _redo_btn = Gtk.Template.Child()

    _new_action: Gio.SimpleAction
    _open_action: Gio.SimpleAction
    _run_action: Gio.SimpleAction
    _save_action: Gio.SimpleAction
    _save_as_action: Gio.SimpleAction
    _search_action: Gio.SimpleAction

    buffer: lib.ProjectBuffer
    compiler: lib.Compiler
    icon_colors: icon.CommandColorScheme
    icon_colors_dark: icon.CommandColorScheme
    debugger: Debugger
    updater: Updater

    _use_csd: bool = False
    _titlebar: Gtk.HeaderBar

    @GObject.Property(type=bool, default=False)
    def use_csd(self):
        return self._use_csd

    @use_csd.setter
    def use_csd(self, value):
        if value == self._use_csd:
            return
        self._use_csd = value and os.environ.get('GTK_CSD', '1') != '0'

        if self._toolbar_left_box.props.parent is not None:
            self._toolbar_left_box.props.parent.remove(
                self._toolbar_left_box)
        if self._toolbar_right_box.props.parent is not None:
            self._toolbar_right_box.props.parent.remove(
                self._toolbar_right_box)

        self.set_titlebar(None)
        self._titlebar = None

        if self._use_csd:
            self._titlebar = Gtk.HeaderBar.new()
            self._titlebar.pack_start(self._toolbar_left_box)
            self._titlebar.pack_end(self._toolbar_right_box)
            self.set_titlebar(self._titlebar)
        else:
            self._toolbar_box.append(self._toolbar_left_box)
            self._toolbar_box.append(self._toolbar_right_box)

        self._toolbar_box.props.visible = not self._use_csd

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.debugger = None
        self.buffer = lib.ProjectBuffer()

        self._titlebar = None

        self.compiler = lib.Compiler(self.buffer)

        self.icon_colors = icon.get_default_colors()
        self.icon_colors_dark = icon.get_default_colors_dark()
        # Settings
        gtk_settings = self.get_settings()
        self.props.application.settings.bind(
            'dark-mode', gtk_settings, 'gtk-application-prefer-dark-theme',
            Gio.SettingsBindFlags.GET)
        self.props.application.settings.bind(
            'csd', self, 'use-csd',
            Gio.SettingsBindFlags.GET)
        self.props.application.settings.bind(
            'auto-indent', self._program_view, 'auto-indent',
            Gio.SettingsBindFlags.GET)
        self.props.application.settings.connect(
            'changed', self._on_settings_changed)
        self.props.application.settings.bind(
            'code-preview', self._codepreview_sw, 'visible',
            Gio.SettingsBindFlags.GET)
        self._on_settings_changed(self.props.application.settings, '')

        self._icon_view.props.project_buffer = self.buffer
        self._icon_view.props.window = self
        self._icon_view.props.programview = self._program_view

        self._program_view.props.compiler = self.compiler
        self._program_view.set_codebuffer(self.buffer.code)
        self._program_view.connect(
            'icon-hover', self._on_programview_icon_hover)

        self._program_view_search.props.programview = self._program_view

        self._codepreview.set_programview(self._program_view)

        # Actions
        self._run_action = utils.win_add_action(
            self, 'project.run', 'F5', self._on_run)
        self._open_action = utils.win_add_action(
            self, 'project.open', '<Ctrl>O', self._on_open)
        self._save_action = utils.win_add_action(
            self, 'project.save', '<Ctrl>S', self._on_save)
        self._new_action = utils.win_add_action(
            self, 'project.new', '<Ctrl>N', self._on_new)
        self._save_as_action = utils.win_add_action(
            self, 'project.save-as', '<Ctrl><Shift>S', self._on_save_as)
        self._search_action = utils.win_add_action(
            self, 'search', '<Ctrl>F', self._on_search)
        self._project_properties_action = utils.win_add_action(
            self, 'project.properties', 'F6', self._on_project_properties)
        self._functions_action = utils.win_add_action(
            self, 'project.functions', 'F7', self._on_functions)
        self._scene_editor_action = utils.win_add_action(
            self, 'project.scene-editor', 'F8', self._on_scene_editor)

        self._run_btn_set_running(False)

        # Buffer
        self.buffer.connect('notify::changed', self._on_buffer_changed_notify)
        self.buffer.code.bind_property('can-undo', self._undo_btn, 'sensitive',
                                       GObject.BindingFlags.SYNC_CREATE)
        self.buffer.code.bind_property('can-redo', self._redo_btn, 'sensitive',
                                       GObject.BindingFlags.SYNC_CREATE)
        self._on_buffer_changed_notify(None, None)

        # Updater
        self.updater = Updater(self)
        self.updater.check_for_updates()

    def _on_run(self, action, params):
        if self.debugger and self.debugger.props.running:
            self.debugger.stop()
            return
        self.debugger = Debugger(self.buffer, self.compiler)
        self.debugger.connect('debugging-done', self._on_debugging_done)
        self._run_btn_set_running(True)
        self.debugger.run()

    def _on_open(self, action, params):
        self.open()

    def _on_save(self, action, params):
        def save(ok):
            pass
        self.save(save)

    def _on_new(self, action, params):
        self.new_project()

    def _on_save_as(self, action, params):
        def save_as(ok):
            pass
        self.save_as(save_as)

    def open(self):
        def check(ok):
            if not ok:
                return

            def open(file: Gio.File):
                self.open_local_file(file)

            utils.filedialog(
                self, _('Open project'), Gtk.FileChooserAction.OPEN,
                [lib.MIME_TURTLICO_PROJECT_FILTER], open)
        self._check_saved(check)

    def open_local_file(self, file: Gio.File):
        self.notifications.clear()
        if file is None:
            return

        def error(e):
            self.notifications.add_simple(
                _('Cannot load project: ') + str(e), Gtk.MessageType.ERROR)
            self.buffer.load_from_file(None)

        ext = os.path.splitext(file.get_basename())[1]
        if ext != '.tcp':
            error(Exception(_('Wrong file extenstion')))
            return
        if not file.query_exists():
            error(
                Exception(_('File "{}" is not available').format(
                    file.get_uri())))
            return

        try:
            self.buffer.load_from_file(file)
        except lib.CorruptedFileException as e:
            error(e)
        except lib.MissingPluginException as e:
            error(e)
        except lib.UnavailableCommandException as e:
            error(e)
        except GLib.Error as e:
            error(e)

    def save(self, callback: Callable[[bool], None]):
        """
        Args:
            callback (Callable[[bool], None]): False if saving failed
        """
        if self.buffer.project_file is not None:
            try:
                self.buffer.save()
                callback(True)
            except GLib.Error:
                callback(False)
            return
        self.save_as(callback)

    def save_as(self, callback: Callable[[bool], None]):
        def save(file: Gio.File):
            if file is None:
                callback(False)
                return
            try:
                ok = self.buffer.save_as(file)
                callback(ok)
            except GLib.Error:
                callback(False)
        utils.filedialog(
            self, _('Save project as'), Gtk.FileChooserAction.SAVE,
            [lib.MIME_TURTLICO_PROJECT_FILTER], save)

    def _check_saved(self, callback: Callable[[bool], None]):
        """Checks whether the project is saved.
        If it is not then it asks the user whether they want to save it.
        If cancel option is chosen then returns False and the caller should
        stop its operation.
        """
        if not self.buffer.props.changed:
            callback(True)
            return
        dialog = Gtk.MessageDialog(
            modal=True,
            transient_for=self,
            text=(_('The program contains unsaved changed.') + '\n'
                  + _('Would you like to save the project?')),
            secondary_text=_('Otherwise the unsaved changed will be lost.'),
            message_type=Gtk.MessageType.QUESTION)
        dialog.add_buttons(
            _('Yes'), Gtk.ResponseType.YES,
            _('No'), Gtk.ResponseType.NO,
            _('Cancel'), Gtk.ResponseType.CANCEL)

        def response(dialog, response):
            dialog.close()
            if response == Gtk.ResponseType.YES:
                self.save(callback)
                return
            elif response == Gtk.ResponseType.NO:
                callback(True)
                return
            callback(False)

        dialog.connect('response', response)
        dialog.show()

    def new_project(self):
        def callback(ok):
            if not ok:
                return
            self.buffer.load_from_file(None)
        self._check_saved(callback)

    def _on_buffer_changed_notify(self, obj, prop):
        self._save_action.set_enabled(self.buffer.props.changed)
        self._update_window_title()

    def _update_window_title(self):
        project = self.buffer.props.project_file
        if project is None:
            file_name = _('Unnamed')
        else:
            file_name = os.path.splitext(project.get_basename())[0]
        changed = '● ' if self.buffer.props.changed else ''

        self.props.title = f'{changed}{file_name} - Turtlico'

    @Gtk.Template.Callback()
    def _on_close_request(self, win) -> bool:
        if self.buffer.props.changed is False:
            return False

        def close(ok):
            if ok:
                self.buffer.props.changed = False
                self.close()
        self._check_saved(close)
        return True

    def _run_btn_set_running(self, running: bool):
        if running:
            self._run_btn_img.props.icon_name = 'media-playback-stop-symbolic'
            self._run_btn.props.tooltip_text = _('Stop')
        else:
            self._run_btn_img.props.icon_name = 'media-playback-start-symbolic'
            self._run_btn.props.tooltip_text = _('Run')

    def _on_debugging_done(self, debugger, result: DebuggingReuslt):
        self.debugger.dispose()
        self.debugger = None
        self._run_btn_set_running(False)

        if result.props.program_failed:
            dialog = Gtk.MessageDialog(
                transient_for=self,
                modal=True,
                buttons=Gtk.ButtonsType.OK,
                text=_('Program crashed'),
                secondary_text=result.props.error_message,
                message_type=Gtk.MessageType.ERROR)
            dialog.show()

            def close(dialog, reposne):
                dialog.close()

            dialog.connect('response', close)

    @Gtk.Template.Callback()
    def _on_program_view_search_close_request(self, obj):
        self._program_view_search_rev.props.reveal_child = False

    @Gtk.Template.Callback()
    def _on_undo_btn_clicked(self, btn):
        self._program_view.get_codebuffer().undo()

    @Gtk.Template.Callback()
    def _on_redo_btn_clicked(self, btn):
        self._program_view.get_codebuffer().redo()

    def _on_settings_changed(self, settings: Gio.Settings, key: str):
        uall = key == ''
        if key == 'dark-icons' or uall:
            colors = (
                self.icon_colors_dark if settings.get_boolean('dark-icons')
                else self.icon_colors
            )
            self._program_view.set_colors(colors)
            self._icon_view.set_colors(colors)
            self._program_view_search.pw_find.set_colors(colors)
            self._program_view_search.pw_replace.set_colors(colors)

    def _on_search(self, action, params):
        self._program_view_search.clear()
        self._program_view_search_rev.props.reveal_child = (
            not self._program_view_search_rev.props.reveal_child
        )

    def _on_project_properties(self, action, params):
        self._icon_view.sidebar_project_props_row.activate()

    def _on_functions(self, action, params):
        self._icon_view.sidebar_functions_window_row.activate()

    def _on_scene_editor(self, action, params):
        self._icon_view.sidebar_scene_editor_row.activate()

    def _on_programview_icon_hover(self, programview):
        if self._program_view.icon_hovered is None:
            self._status_bar.props.label = ''
            self._codepreview.unselect()
            return
        cx, cy, cmd = self._program_view.icon_hovered
        self._status_bar.props.label = '{}:{} {}'.format(
            cy + 1, cx + 1, cmd.definition.help)
        self._codepreview.scroll_to(cx, cy)
