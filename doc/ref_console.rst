============
Console docs
============
.. container:: cmd
   :name: c-print

   **Print to console**

   Prints text (object/s) to console and starts a new line.

   *Parameters*

   * ``STRING`` The text
   * ``STRING`` ...
.. container:: cmd
   :name: c-input

   **Input from console**

   *Parameters*

   * ``STRING`` Prompt text (optional)
.. container:: cmd
   :name: c-print-nnl

   **Print to console without newline**

   Prints text (object/s) to console but does not start a new line.

   *Parameters*

   * ``STRING`` The text
   * ``STRING`` ...
.. container:: cmd
   :name: c-stderr

   **Print to stderr (option)**

   Prints text to stderr (standard error) instead of stdout (standard output).
   You can use this command as a parameter
   for `print <#c-print>`__ and `print without newline <#c-print-nnl>`__.
