# flake8: noqa
from turtlico.lib import Plugin, CommandCategory
from turtlico.lib import CommandDefinition, CommandType, CommandModule, CommandEvent
from turtlico.lib.icon import icon, text
from turtlico.locale import _

def get_plugin():
    p = Plugin(_('Multimedia'), doc_url='ref_mm.html')
    p.categories = [
        CommandCategory(p, icon('multimedia.svg'), [
            CommandDefinition('mm_ef_s', icon('multimedia.svg'), _('Create sound player for a file'), CommandType.METHOD, 'tcf_mm_player', ''),
            CommandDefinition('mm_tone', text('🎺'), _('Play tone'), CommandType.METHOD, 'tcf_mm_tone', '', snippet='mm_tone;(;str,A;sep;int,500;sep;true;);'),
            CommandDefinition('mm_ps', text('▶️'), _('Play sound (file or sound player)'), CommandType.METHOD, 'tcf_mm_play_sound', ''),
            CommandDefinition('mm_ss', text('⏸️'), _('Pause sound player'), CommandType.METHOD, 'tcf_mm_player_pause', ''),
            CommandDefinition('mm_ef_s_status', text('?⏯️'), _('Get is player playing'), CommandType.METHOD, 'tcf_mm_player_get_playing', ''),
            CommandDefinition('mm_ef_s_seek', text('⏩'), _('Seek to given position in seconds'), CommandType.METHOD, 'tcf_mm_player_seek', ''),
            CommandDefinition('mm_ef_s_pos', text('?⏩'), _('Get current position'), CommandType.METHOD, 'tcf_mm_player_pos', ''),
            CommandDefinition('mm_ef_s_dur', text('?⌛'), _('Get the duration of the media opened in the player (zero if duration is unknown)'), CommandType.METHOD, 'tcf_mm_player_duration', ''),
        ])
    ]
    p.modules = {
        'mm': CommandModule(
            deps=(),
            code="""import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst
Gst.init(sys.argv)"""
        ),
        'tcf_mm_play_sound': CommandModule(
            deps=(),
            code="""def tcf_mm_play_sound(file, volume=1.0):
	if isinstance(file, str):
		pl = Gst.ElementFactory.make('playbin', None)
		if Gst.uri_is_valid(file):
			uri = file
		else:
			uri = Gst.filename_to_uri(file)
		pl.set_property('uri', uri)
		pl.set_property('volume', volume)
		pl.set_state(Gst.State.PLAYING)
	else:
		file.set_property('volume', volume)
		file.set_state(Gst.State.PLAYING)"""
        ),
        'tcf_mm_player': CommandModule(
            deps=(),
            code="""def tcf_mm_player(file):
	pl = Gst.ElementFactory.make('playbin', None)
	if Gst.uri_is_valid(file):
		uri = file
	else:
		uri = Gst.filename_to_uri(file)
	pl.set_property('uri', uri)
	return pl"""
        ),
        'tcf_mm_tone': CommandModule(
            deps=('tcf_sleep',),
            code="""def tcf_mm_tone(frequency, duration = 500, wait_for_end = True):
	if isinstance(frequency, str):
		frequencies = {{'C': 261.6, 'D': 293.66, 'E': 329.62, 'F': 349.22, 'G': 391.99, 'A': 440, 'B': 493.88}}
		frequency = frequencies[frequency]
	pl = Gst.Pipeline(name='note')
	source = Gst.ElementFactory.make('audiotestsrc', 'src')
	sink = Gst.ElementFactory.make('autoaudiosink', 'output')
	source.set_property('freq', frequency)
	pl.add(source)
	pl.add(sink)
	source.link(sink)
	pl.set_state(Gst.State.PLAYING)
	def stop():
		pl.set_state(Gst.State.NULL)
		return False
	if wait_for_end:
		try:
			tcf_sleep(duration / 1000)
		except NameError:
			time.sleep(duration / 1000)
		stop()
	elif duration != 0:
		try:
			ontimer(stop, duration)
		except NameError:
			raise Exception('{}')
	return pl""".format(_("Parameter wait_for_end=False is not supported without Turtle plugin.")),
            deps_required=False
        ),
        'tcf_mm_player_pause': CommandModule(
            deps=(),
            code="""def tcf_mm_player_pause(pl):
	pl.set_state(Gst.State.PAUSED)"""
        ),
        'tcf_mm_player_get_playing': CommandModule(
            deps=(),
            code="""def tcf_mm_player_get_playing(pl):
	state = pl.get_state(Gst.CLOCK_TIME_NONE)
	if state[0] == Gst.StateChangeReturn.SUCCESS:
		return state[1] == Gst.State.PLAYING
	else:
		return False"""
        ),
        'tcf_mm_player_seek': CommandModule(
            deps=('tcf_mm_player_duration',),
            code="""def tcf_mm_player_seek(pl, time_secs):
	if time_secs < 0:
		raise Exception('{0}')
	if time_secs > tcf_mm_player_duration(pl):
		raise Exception('{1}')
	pl.seek_simple(Gst.Format.TIME,  Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT, time_secs * Gst.SECOND)""".format(_('Seek position smaller than 0'), _('Seek position greater than the duration of the media'))
        ),
        'tcf_mm_player_pos': CommandModule(
            deps=(),
            code="""def tcf_mm_player_pos(pl):
	position = pl.query_position(Gst.Format.TIME)
	if position[0] and position[1] != None:
		return position[1] / Gst.SECOND
	else:
		return 0"""
        ),
        'tcf_mm_player_duration': CommandModule(
            deps=(),
            code="""def tcf_mm_player_duration(pl):
	position = pl.query_duration(Gst.Format.TIME)
	if position[0] and position[1] != None:
		return position[1] / Gst.SECOND
	else:
		return 0"""
        ),
    }
    return p
