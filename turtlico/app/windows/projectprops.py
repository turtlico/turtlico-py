# Copyright (C) 2021 saytamkenorh
#
# This file is part of Turtlico.
#
# Turtlico is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turtlico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Turtlico.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

import os

from gi.repository import GObject, GLib, Gio, Gtk, Gdk

import turtlico.lib as lib
import turtlico.app.widgets as widgets
import turtlico.utils as utils
from turtlico.locale import _


class ProjectPropsPlugin(GObject.Object):
    __gtype_name__ = 'TurtlicoProjectPropsPlugin'

    plugin: lib.Plugin
    widget: widgets.PropertySwitch

    _project: lib.ProjectBuffer

    def __init__(self, plugin: lib.Plugin, project: lib.ProjectBuffer):
        super().__init__()

        self.plugin = plugin
        self._project = project

        self.widget = widgets.PropertySwitch()
        self.widget.title = plugin.name

        self._set_state()

        self.widget.switch.connect('notify::active', self._on_state_changed)

    def _set_state(self, *args):
        with self.widget.switch.freeze_notify():
            self.widget.switch.props.active = (
                self.plugin.id in self._project.enabled_plugins.keys())

    def _on_state_changed(self, obj, prop) -> bool:
        try:
            self._project.set_plugin_enabled(
                self.plugin.id, self.widget.switch.props.active)
        except lib.RemovedUsedPluginException as e:
            dialog = Gtk.MessageDialog(
                modal=True,
                transient_for=utils.get_parent_window(self.widget),
                text=_('Cannot disable the plugin'),
                secondary_text=str(e),
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.OK
            )

            def response(dialog, reposne):
                dialog.close()
            dialog.connect('response', response)

            dialog.show()
        self._set_state()


@Gtk.Template(resource_path='/io/gitlab/Turtlico/ui/projectprops.ui')
class ProjectPropsWindow(Gtk.Dialog):
    __gtype_name__ = 'TurtlicoProjectPropsWindow'

    _run_in_console_switch: widgets.PropertySwitch = Gtk.Template.Child()
    _plugins_listbox: Gtk.ListBox = Gtk.Template.Child()

    _project: lib.ProjectBuffer
    _plugins_store: Gio.ListStore

    def __init__(self, project: lib.ProjectBuffer, parent: Gtk.Window):
        super().__init__(
            transient_for=parent,
            modal=True,
        )
        self._project = project

        self._project.bind_property(
            'run_in_console', self._run_in_console_switch.switch, 'active',
            (GObject.BindingFlags.BIDIRECTIONAL
             | GObject.BindingFlags.SYNC_CREATE))

        self._plugins_store = Gio.ListStore.new(ProjectPropsPlugin)
        self._plugins_listbox.bind_model(
            self._plugins_store, self._plugins_listbox_widget_create)

        self.reload_plugins()

    def _plugins_listbox_widget_create(
            self, item: ProjectPropsPlugin) -> Gtk.Widget:
        return item.widget

    @Gtk.Template.Callback()
    def _on_plugin_folder_btn_clicked(self, btn):
        folder = os.path.join(
            GLib.get_user_data_dir(), 'turtlico/turtlico/plugins')

        try:
            if os.path.exists(folder):
                if not os.path.isdir(folder):
                    raise Exception(
                        _('Path "{}" does exist but it is not a folder.'))
            else:
                os.makedirs(folder)
        except Exception as e:
            dialog = Gtk.MessageDialog(
                modal=True,
                transient_for=self,
                text=str(e),
                buttons=Gtk.ButtonsType.OK)

            def callback(dialog, reposne):
                dialog.close()
            dialog.connect('response', callback)

            dialog.show()
            return

        Gtk.show_uri(self, f'file://{folder}', Gdk.CURRENT_TIME)

    def reload_plugins(self):
        self._plugins_store.remove_all()

        plugins = lib.Plugin.get_all()
        for plid, plugin in plugins.items():
            if plid == 'base':
                continue
            switch = ProjectPropsPlugin(plugin, self._project)
            self._plugins_store.append(switch)

        def sort(
                a: ProjectPropsPlugin,
                b: ProjectPropsPlugin) -> int:
            return GLib.strcmp0(a.plugin.name, b.plugin.name)
        self._plugins_store.sort(sort)
