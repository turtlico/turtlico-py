========
Examples
========

- `Calculator <./examples/calc.tcp>`_
- `Color gradient <./examples/color.tcp>`_
- `Console <./examples/console.tcp>`_
- `Direct Python <./examples/direct-python.tcp>`_
- `Exceptions <./examples/exceptions.tcp>`_
- `Files example <./examples/files.tcp>`_
- `Image example <./examples/image.tcp>`_
- `Lists <./examples/lists.tcp>`_
- `Mouse click counter <./examples/click-counter.tcp>`_
- `Random move <./examples/random-move.tcp>`_
- `Scenes <./examples/scenes.zip>`_
- `Sound player <./examples/multimedia.tcp>`_
- `Turtle controlled by keyboard and mouse <./examples/keyboard-mouse.tcp>`_
- `Turtle star <./examples/turtle-star.tcp>`_
- `Write user input to file <./examples/files-write-input.tcp>`_
