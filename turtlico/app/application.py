# application.py
#
# Copyright 2020 saytamkenorh
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import Union
import urllib.parse
import os
import gi

gi.require_version('Gdk', '4.0')
gi.require_version('Graphene', '1.0')
gi.require_version('Gtk', '4.0')
gi.require_version('Rsvg', '2.0')

from gi.repository import GLib, Gtk, Gio, Gdk

import turtlico.app.windows as windows
import turtlico.app.cli as cli
import turtlico.utils as utils

from turtlico.locale import _


class Application(Gtk.Application):
    _DOC_URL = 'https://turtlico.gitlab.io/doc/'

    _main_window: windows.MainWindow
    settings: Gio.Settings
    version: str

    def __init__(self, version: str):
        super().__init__(application_id='io.gitlab.Turtlico',
                         flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE)
        self._main_window = None
        self.version = version
        self.settings = Gio.Settings.new('io.gitlab.Turtlico')
        Gtk.Window.set_default_icon_name('io.gitlab.Turtlico')

        # CLI options
        self.set_option_context_parameter_string('[{}]'.format(_('FILE')))
        self.add_main_option(
            'version', b'v', GLib.OptionFlags.NONE, GLib.OptionArg.NONE,
            _('Show version of the application'), None)
        self.add_main_option(
            'compile', b'c', GLib.OptionFlags.NONE, GLib.OptionArg.STRING,
            _('Compile file and save ouput to FILE (overwrites if exists)'),
            _('FILE'))

    @staticmethod
    def get_doc_uri() -> str:
        from os.path import dirname as dn
        local_docs = os.path.join(dn(dn(dn(dn(__file__)))), 'doc/turtlico/')
        if os.path.isdir(local_docs):
            docs = f'file://{local_docs}'
        else:
            docs = Application._DOC_URL
        return urllib.parse.urljoin(docs, 'en/')

    def do_startup(self):
        Gtk.Application.do_startup(self)

        # Init CSS
        css_provider = Gtk.CssProvider.new()
        css_provider.load_from_resource('/io/gitlab/Turtlico/css/turtlico.css')
        display = Gdk.Display.get_default()
        Gtk.StyleContext.add_provider_for_display(
            display, css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        # Init actions
        self._set_actions()

    def do_activate(self):
        self.make_window().present()

    def make_window(self):
        win = self.props.active_window
        if not win:
            win = windows.MainWindow(application=self)
            self._main_window = win
        return win

    def do_command_line(self, command_line):
        options: GLib.VariantDict = command_line.get_options_dict()

        input_file: Union[Gio.File, None] = None

        argc = len(command_line.get_arguments())
        if argc == 2:
            input_file = Gio.File.new_for_commandline_arg(
                command_line.get_arguments()[1])
            if not input_file.query_exists():
                utils.warn(_('Input file does not exist'))
        elif argc > 2:
            utils.error(_('Please specify only one input file'))
            return 1

        if options.contains('compile'):
            outf = options.lookup_value('compile', None).get_string()
            return cli.compile(input_file, outf)

        try:
            win = self.make_window()
        except Exception:
            self.quit()
            raise
        win.present()
        if input_file is not None:
            win.open_local_file(input_file)

        return 0

    def do_handle_local_options(self, options: GLib.VariantDict):
        if options.contains('version'):
            print(f'Turtlico {self.version}')
            return 0
        return -1

    def _set_actions(self):
        action_entries = [
            ('open-settings', self._settings, None),
            ('show-about-dialog', self._about, None),
            ('show-help', self._help, ('app.show-help', ['<Shift>F1'])),
            ('shortcuts', self._shortcuts, ('app.shortcuts', ['F1'])),
            ('quit', self._quit, ('app.quit', ['<Ctrl>Q']))
        ]

        for action, callback, accel in action_entries:
            simple_action = Gio.SimpleAction.new(action, None)
            simple_action.connect('activate', callback)
            self.add_action(simple_action)
            if accel is not None:
                self.set_accels_for_action(*accel)

    def _about(self, action, params):
        about_dialog = windows.AboutDialog(self, self._main_window)
        about_dialog.show()

    def _settings(self, action, params):
        settings_dialog = windows.AppSettingsWindow(self, self._main_window)
        settings_dialog.show()

    def _help(self, action, params):
        Gtk.show_uri(
            self._main_window,
            urllib.parse.urljoin(Application.get_doc_uri(), 'index.html'),
            Gdk.CURRENT_TIME)

    def _shortcuts(self, actions, params):
        sw = windows.ShortcutsWindow()
        sw.props.modal = True
        sw.props.transient_for = self._main_window
        sw.show()

    def _quit(self, action, params):
        if self._main_window:
            self._main_window.close()
