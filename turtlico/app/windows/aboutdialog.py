# aboutdialog.py
#
# Copyright 2021 saytamkenorh
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

from gi.repository import Gtk

from turtlico.locale import _
import turtlico.app as app


class AboutDialog(Gtk.AboutDialog):
    def __init__(self, application: app.Application, parent: Gtk.Window):
        translator_credits = _('[Name of translators]')
        if translator_credits == '[Name of translators]':
            translator_credits = None

        super().__init__(
            logo_icon_name='io.gitlab.Turtlico',
            program_name=_('Turtlico'),
            version=application.version,
            comments=_('The easy programming tool'),
            website='https://turtlico.gitlab.io',
            license_type=Gtk.License.GPL_3_0,

            authors=['saytamkenorh https://gitlab.com/matyas5'],
            copyright='Copyright © 2018-2021 saytamkenorh',

            translator_credits=translator_credits,

            transient_for=parent,
            modal=True,
        )
