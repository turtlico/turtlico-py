# Copyright (C) 2021 saytamkenorh
#
# This file is part of Turtlico.
#
# Turtlico is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turtlico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Turtlico.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

import json
import os
import subprocess
import sys
import tempfile
import threading
import urllib.error
import urllib.parse
import urllib.request

from gi.repository import GLib, Gtk, Gio

import turtlico.app as app
import turtlico.utils as utils
import turtlico.app.widgets as widgets
from turtlico.locale import _


def version_cmp(version1, version2) -> int:
    versions1 = [int(v) for v in version1.split(".")]
    versions2 = [int(v) for v in version2.split(".")]
    for i in range(max(len(versions1), len(versions2))):
        v1 = versions1[i] if i < len(versions1) else 0
        v2 = versions2[i] if i < len(versions2) else 0
        if v1 > v2:
            return 1
        elif v1 < v2:
            return -1
    return 0


class Updater():
    UPDATE_URL = 'https://turtlico.gitlab.io/'

    _mainwindow: app.MainWindow
    _download_cancellable: Gio.Cancellable

    def __init__(self, main_window: app.MainWindow):
        self._mainwindow = main_window
        self._download_cancellable = Gio.Cancellable.new()

    def check_for_updates(self):
        if sys.platform != 'win32':
            return
        thread = threading.Thread(
            target=self._check_for_updates,
            args=[self._mainwindow.props.application.version], daemon=True)
        thread.start()

    def _check_for_updates(self, local_version):
        builds_url = urllib.parse.urljoin(self.UPDATE_URL, 'builds.json')
        try:
            with urllib.request.urlopen(builds_url) as f:
                builds_data = f.read().decode('utf-8')
                builds = json.loads(builds_data)
        except json.JSONDecodeError as e:
            utils.warn(f'Cannot check for updates: Invalid json file: {e}')
            return
        except urllib.error.URLError as e:
            utils.warn(f'Cannot check for updates: {e}')
            return
        except urllib.error.HTTPError as e:
            utils.warn(f'Cannot check for updates: {e}')
            return

        remote_version = builds.get('version', None)
        if remote_version is None:
            utils.warn('Cannot check for updates: Invalid json file')
            return

        if version_cmp(remote_version, local_version) <= 0:
            # App is up-to date
            return

        GLib.idle_add(
            self._show_available_updates, local_version, remote_version)

    def _show_available_updates(self, local_version, remote_version):
        notif = Gtk.InfoBar.new()
        notif.props.show_close_button = True
        notif.props.message_type = Gtk.MessageType.INFO

        label = Gtk.Label.new(
            _('New version of Turtlico is available: "{}". Installed version: "{}".'  # noqa: E501
              ).format(remote_version, local_version))
        notif.add_child(label)

        update_btn = notif.add_button(_('Update'), Gtk.ResponseType.APPLY)
        update_btn.connect('clicked', self._on_update_btn_clicked, notif)

        self._mainwindow.notifications.add(notif)

    def _on_update_btn_clicked(self, btn, notif: Gtk.InfoBar):
        notif.unparent()
        self.perform_update()

    def perform_update(self):
        win = UpdateProgressWindow()
        win.props.application = self._mainwindow.props.application
        win.props.transient_for = self._mainwindow
        win.connect('close-request', self._on_close_request)
        win.show()

        thread = threading.Thread(
            target=self._perform_update,
            args=[win], daemon=True)
        thread.start()

    def _perform_update(self, win: UpdateProgressWindow):
        self._download_cancellable.reset()

        installer_url = urllib.parse.urljoin(
            self.UPDATE_URL, 'turtlico-windows.exe')
        try:
            installer_file = open(
                os.path.join(
                    tempfile.gettempdir(), 'turtlico-windows.exe'), 'w+b')
        except OSError as e:
            GLib.idle_add(
                win.show_error,
                _('Cannot download update installer:\n{}').format(str(e)))
            return
        try:
            with urllib.request.urlopen(installer_url) as f:
                length = f.getheader('content-length')
                if length:
                    length = int(length)
                    buf_size = min(8192, length)
                else:
                    buf_size = 8192
                    length = None

                bytes_read = 0
                while len(buf := f.read(buf_size)) > 0:
                    installer_file.write(buf)
                    bytes_read += len(buf)
                    GLib.idle_add(
                        win.download_progress,
                        min(bytes_read / length, 1) if length else 0)
                    if self._download_cancellable.is_cancelled():
                        GLib.idle_add(win.close)
                        installer_file.close()
                        os.remove(installer_file.name)
                        return
        except OSError as e:
            GLib.idle_add(
                win.show_error,
                _('Cannot download update installer:\n{}').format(str(e)))
            return
        installer_file.close()

        # Executes the installer
        def format_msg(error):
            return _('Cannot start update installer:\n{}').format(str(error))
        try:
            subprocess.Popen(
                [installer_file.name, '/SILENT', '/CLOSEAPPLICATIONS', '/SP-'])
        except OSError as e:
            GLib.idle_add(win.show_error, format_msg(e))
            return
        except ValueError as e:
            GLib.idle_add(win.show_error, format_msg(e))
            return
        except subprocess.SubprocessError as e:
            GLib.idle_add(win.show_error, format_msg(e))
            return

        GLib.idle_add(win.close)
        GLib.idle_add(self._mainwindow.close)

    def _on_close_request(self, win) -> bool:
        self._download_cancellable.cancel()
        return False


@Gtk.Template(resource_path='/io/gitlab/Turtlico/ui/updateprogresswindow.ui')
class UpdateProgressWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'TurtlicoUpdateProgressWindow'

    _label: Gtk.Label = Gtk.Template.Child()
    _progress_bar: Gtk.ProgressBar = Gtk.Template.Child()
    notifications: widgets.AppNotifications = Gtk.Template.Child()

    def __init__(self) -> None:
        super().__init__()

    def download_progress(self, fraction: float):
        self._label.props.label = _('Downloading update installer')
        if not fraction:
            self._progress_bar.pulse()
        else:
            self._progress_bar.props.fraction = fraction

    def show_error(self, msg: str):
        self.notifications.add_simple(msg, Gtk.MessageType.ERROR, False)
        self._label.props.label = _('Update failed')
        self._progress_bar.props.fraction = 1
