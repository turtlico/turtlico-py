# Copyright (C) 2021 saytamkenorh
#
# This file is part of Turtlico.
#
# Turtlico is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turtlico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Turtlico.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

import os
import json
import math
import threading
from typing import Union

from gi.repository import GObject, GLib, Gio, Gdk

import turtlico.lib as lib
import turtlico.utils as utils
from turtlico.locale import _


class SceneInfo(GObject.Object):
    name = GObject.Property(type=str)
    project_prefix: str
    file = GObject.Property(type=Gio.File)

    @GObject.Signal
    def deleted(self):
        """Emitted after deleting the scene"""
        pass

    @GObject.Signal
    def renamed(self):
        """Emitted after renaming the scene"""
        pass

    def __init__(self, project_prefix: str, file: Gio.File):
        super().__init__()
        self.project_prefix = project_prefix
        self.file = file
        self._update_name()

    def rename(self, new_name: str):
        if new_name == self.props.name:
            return
        self.props.file = (
            self.props.file.set_display_name(
                f'{self.project_prefix}{new_name}.tcs')
        )
        self.emit('renamed')

    def delete(self):
        self.props.file.delete()
        self.emit('deleted')

    def _update_name(self):
        name, ext = os.path.splitext(self.props.file.get_basename())
        self.props.name = name.replace(
            self.project_prefix, '', 1)

    @staticmethod
    def compare(a: SceneInfo, b: SceneInfo) -> int:
        return GLib.strcmp0(a.props.name, b.props.name)

    @staticmethod
    def equal(a: SceneInfo, b: SceneInfo) -> bool:
        if a is None or b is None:
            return False
        return a.props.file.equal(b.props.file)


class SpriteInfo(GObject.Object):
    name: str
    texture = GObject.Property(type=Gdk.Texture)
    texture_file: Gio.File
    texture_time: int

    def __init__(
            self, name: str,
            texture: Union[Gio.File, Gdk.Texture],
            texture_time: int = 0) -> None:
        super().__init__()
        self.name = name
        self.texture_time = texture_time

        if isinstance(texture, Gio.File):
            self.props.texture = None
            self.texture_file = texture
        elif isinstance(texture, Gdk.Texture):
            self.props.texture = texture
            self.texture_file = None
        else:
            raise Exception(
                'texture must be either a Gio.File or a Gdk.Texture')

    def request_texture(self):
        if self.texture is not None:
            return
        if self.texture_file is None:
            raise Exception(
                'Trying to request texture without texture_file set')
        t = threading.Thread(
            target=self._load_texture, args=(self.texture_file.dup(),))
        t.daemon = True
        t.start()

    def _load_texture(self, file: Gio.File):
        try:
            texture = Gdk.Texture.new_from_file(file)
        except GLib.Error as e:
            utils.warn(
                f'Cannot load texture of sprite "{self.name}": {e.message}')
            return
        GLib.idle_add(self._set_texture, texture)

    def _set_texture(self, texture: Gdk.Texture):
        self.props.texture = texture


class SceneSprite(GObject.Object):
    x: int
    y: int
    name: str
    id: str


class SceneBuffer(GObject.Object):
    """Contains scene data"""

    __gtype_name__ = "SceneBuffer"

    project: lib.ProjectBuffer
    _project_file: Gio.File
    _project_prefix: str
    _batch_changes: bool
    _batch_changes_changed: bool

    scenes: list[SceneInfo]
    sprites: dict[str, SpriteInfo]

    scene_name: Union[str, None]
    _scene_width: int
    _scene_height: int
    scene_modified = GObject.Property(type=bool, default=False)
    scene_sprites: list[SceneSprite]

    sprite_turtle: SpriteInfo

    @GObject.Signal
    def project_scan(self):
        """Emitted after a project scan.
        New scenes or sprites might be available."""
        pass

    @GObject.Signal(arg_types=(GLib.Error,))
    def project_scan_failed(self, *args):
        """Emitted when a project scan fails"""
        pass

    @GObject.Signal
    def scene_changed(self):
        """Emitted when scene a new scene is loaded"""
        self.props.scene_modified = False

    @GObject.Signal
    def content_changed(self):
        """Emitted when sprites are changed."""
        self.props.scene_modified = True

    @GObject.Signal
    def sprite_texture_changed(self):
        """Emitted when texture of a sprite was changed"""

    @GObject.Property(type=int)
    def scene_width(self):
        return self._scene_width

    @scene_width.setter
    def scene_width(self, value):
        old_width = self._scene_width
        self._scene_width = value
        self.props.batch_changes = True
        for s in self.scene_sprites:
            x = s.x + old_width / 2 - value / 2
            self.move_sprite(s, x, s.y)
        self._content_changed()
        self.props.batch_changes = False

    @GObject.Property(type=int)
    def scene_height(self):
        return self._scene_height

    @scene_height.setter
    def scene_height(self, value):
        old_height = self._scene_height
        self._scene_height = value
        self.props.batch_changes = True
        for s in self.scene_sprites:
            y = s.y + old_height / 2 - value / 2
            self.move_sprite(s, s.x, y)
        self._content_changed()
        self.props.batch_changes = False

    @GObject.Property(type=bool, default=False)
    def batch_changes(self):
        return self._batch_changes

    @batch_changes.setter
    def batch_changes(self, value):
        self._batch_changes = value
        if self._batch_changes:
            self._batch_changes_changed = False
        else:
            if self._batch_changes_changed:
                self._content_changed()

    def __init__(self, project: lib.ProjectBuffer):
        super().__init__()
        self.scenes = []
        self._scene_width = 0
        self._scene_height = 0
        self._batch_changes = False
        self._batch_changes_changed = False

        self.scene_sprites = []
        self.sprites = {}
        self._clear_scene()

        self.sprite_turtle = SpriteInfo(
            'turtle', Gdk.Texture.new_from_resource(
                '/io/gitlab/Turtlico/textures/turtle_sprite.png'))

        self.project = project
        self._project_file = project.props.project_file
        try:
            pdir: Gio.File = self._project_file.get_parent()
            self._dir_monitor = pdir.monitor_directory(
                Gio.FileMonitorFlags.WATCH_MOVES)
            self._dir_monitor.connect('changed', self._on_dir_monitor_changed)
        except GLib.Error as e:
            self._dir_monitor = None
            utils.warn('Cannot monitor project directory: ') + e.message

        self.load_scene(None)

    def _clear_scene(self):
        self.scene_name = None
        self.props.scene_width = 0
        self.props.scene_height = 0
        self.scene_sprites.clear()

    def scan_project(self):
        """Scans for available scenes and sprites
        """
        try:
            self.__scan_project()
        except GLib.Error as e:
            self.emit('project-scan-failed', e)

    def _scan_if_not_monitored(self):
        if self._dir_monitor is not None:
            return
        self.scan_project()

    def __scan_project(self):
        pdir: Gio.File = self._project_file.get_parent()
        pfname = os.path.splitext(self._project_file.get_basename())[0]
        self._project_prefix = f'{pfname}.'
        enm = pdir.enumerate_children(
            ','.join([
                Gio.FILE_ATTRIBUTE_STANDARD_NAME,
                Gio.FILE_ATTRIBUTE_TIME_MODIFIED]),
            Gio.FileQueryInfoFlags.NONE, None)

        old_sprites = self.sprites.copy()
        self.scenes.clear()
        self.sprites.clear()
        while finfo := enm.next_file():
            filename = finfo.get_name()
            name, ext = os.path.splitext(filename)
            if ext == '.tcs':
                if not name.startswith(self._project_prefix):
                    continue
                file = enm.get_child(finfo)
                sceneinfo = SceneInfo(self._project_prefix, file)
                sceneinfo.connect('deleted', self._on_scene_deleted)
                sceneinfo.connect('renamed', self._on_scene_renamed)
                self.scenes.append(sceneinfo)
            elif ext in lib.IMAGE_EXTENSIONS:
                texture_time = finfo.get_attribute_uint64(
                    Gio.FILE_ATTRIBUTE_TIME_MODIFIED)
                # Tries to load sprite from previous scan
                info = old_sprites.get(filename, None)
                if info is not None:
                    if info.texture_time != texture_time:
                        info = None
                # If it's outdated or not found
                if info is None:
                    info = SpriteInfo(
                        filename, enm.get_child(finfo), texture_time)
                    info.connect(
                        'notify::texture', self._on_sprite_texture_notify)
                self.sprites[info.name] = info

        # Scenes
        self.scenes.sort(key=lambda s: s.props.name)

        # Sprites
        self.sprites[self.sprite_turtle.name] = self.sprite_turtle
        self.sprites = {
            key: value for key, value in sorted(self.sprites.items())}

        self.emit('project-scan')

        # Request textures
        # Some storages (like NVME SSDs) might be async so
        # requesting all textures at once should be ok
        for s in self.sprites.values():
            s.request_texture()

    def load_scene(self, name: str):
        if name is None:
            self._clear_scene()
            self.emit('scene-changed')
            return

        if name.strip() == '':
            raise Exception('Invalid scene name')

        self.scene_name = name
        pdir: Gio.File = self._project_file.get_parent()
        file = pdir.get_child(f'{self._project_prefix}{name}.tcs')
        if not file.query_exists():
            self.props.scene_width = 1280
            self.props.scene_height = 720
            self.scene_sprites.clear()
            self.emit('scene-changed')
            return

        file_dis = Gio.DataInputStream.new(file.read())
        content = file_dis.read_upto('\0', 1)[0]
        file_dis.close()

        try:
            struct = json.loads(content)
        except json.decoder.JSONDecodeError:
            raise GLib.Error.new_literal(
                0, _('File syntax is corrupted'), -1
            )
        try:
            self.props.scene_width = struct['width']
            self.props.scene_height = struct['height']

            self.scene_sprites.clear()
            for s in struct['sprites']:
                self.add_sprite(s['name'], s['x'], s['y'], s['id'], True)
        except KeyError as e:
            raise GLib.Error.new_literal(
                0, _('Missing entry: ') + str(e), -1
            )

        self.emit('scene-changed')

    def save_scene(self):
        if self.scene_name is None:
            raise Exception('No scene is opened')

        pdir: Gio.File = self._project_file.get_parent()
        file = pdir.get_child(f'{self._project_prefix}{self.scene_name}.tcs')

        struct = {}
        struct['width'] = self.props.scene_width
        struct['height'] = self.props.scene_height

        struct['sprites'] = []
        for s in self.scene_sprites:
            struct_sprite = {}
            struct_sprite['x'] = s.x
            struct_sprite['y'] = s.y
            struct_sprite['name'] = s.name
            struct_sprite['id'] = s.id
            struct['sprites'].append(struct_sprite)

        content = json.dumps(struct)

        outs = file.replace(None, False, Gio.FileCreateFlags.NONE)
        ok, bytes_written = outs.write_all(content.encode('utf-8'))

        self.props.scene_modified = False

    def add_scene(self):
        pdir: Gio.File = self._project_file.get_parent()
        i = 0
        while pdir.get_child(f'{self._project_prefix}{i}.tcs').query_exists():
            i += 1
        name = str(i)

        self.load_scene(name)
        self.save_scene()
        self._scan_if_not_monitored()

    def add_sprite(
            self, name: str, x: int, y: int,
            sprite_id: str = None, noupdate: bool = False):
        """Adds a sprite to the scene

        Args:
            name (str): Name of the sprite
            x (int): X coordinate (0 is in the center of the scene)
            y (int): Y coordinate (0 is in the center of the scene)
            sprite_id (str, optional): Sprite id. Creates one if not specified.
                                       Defaults to None.
            noupdate (bool, optional): Call scene change related events.
                                       Defaults to False.
        """
        sprite = SceneSprite()
        sprite.name = name

        if sprite_id is None:
            idname = os.path.splitext(name)[0]
            spritenum = 0
            while any(
                    s for s in self.scene_sprites
                    if s.id == f'{idname}{spritenum}'):
                spritenum += 1
            sprite.id = f'{idname}{spritenum}'
        else:
            sprite.id = sprite_id
        self.scene_sprites.append(sprite)

        self.move_sprite(sprite, x, y, True)

        if not noupdate:
            self._content_changed()

    def move_sprite(
            self, sprite: lib.SceneSprite,
            x: int, y: int, noupdate: bool = False):
        """Moves sprite to new scene coordinates

        Args:
            sprite (lib.SceneSprite): The sprite
            x (int): The x coord
            y (int): The y coord
        """
        if sprite not in self.scene_sprites:
            raise Exception('Sprite is not present in the scene')
        info = self.sprites.get(sprite.name, None)
        if info is None or info.props.texture is None:
            width = 10
            height = 10
        else:
            width = info.props.texture.get_width()
            height = info.props.texture.get_height()
        sprite.x = round(min(
            max(x, math.floor(-self.props.scene_width / 2 - width / 2)),
            math.ceil(self.props.scene_width / 2 + width / 2)))
        sprite.y = round(min(
            max(y, math.floor(-self.props.scene_height / 2 - height / 2)),
            math.ceil(self.props.scene_height / 2 + height / 2)))
        self._content_changed()

    def rename_sprite(self, sprite: lib.SceneSprite, new_id: str):
        if sprite not in self.scene_sprites:
            raise Exception('Sprite is not present in the scene')
        sprite.id = new_id
        self._content_changed()

    def move_sprite_up(self, sprite: lib.SceneSprite):
        if sprite not in self.scene_sprites:
            raise Exception('Sprite is not present in the scene')
        pos = self.scene_sprites.index(sprite)
        if pos >= len(self.scene_sprites) - 1:
            raise Exception('Cannot move sprite higher')
        self.scene_sprites[pos], self.scene_sprites[pos + 1] = (
            self.scene_sprites[pos + 1], self.scene_sprites[pos])
        self._content_changed()

    def move_sprite_down(self, sprite: lib.SceneSprite):
        if sprite not in self.scene_sprites:
            raise Exception('Sprite is not present in the scene')
        pos = self.scene_sprites.index(sprite)
        if pos == 0:
            raise Exception('Cannot move sprite lower')
        self.scene_sprites[pos], self.scene_sprites[pos - 1] = (
            self.scene_sprites[pos - 1], self.scene_sprites[pos])
        self._content_changed()

    def replace_sprite_name(self, old_name: str, new_name: str):
        # Replace in current scene
        for s in self.scene_sprites:
            if s.name == old_name:
                s.name = new_name
                self._content_changed()

        # Replace in saved scenes
        for s in self.scenes:
            file_dis = Gio.DataInputStream.new(s.file.read())
            content = file_dis.read_upto('\0', 1)[0]
            file_dis.close()

            try:
                struct = json.loads(content)
            except json.decoder.JSONDecodeError:
                utils.warn(
                    f'Found invalid "{s.props.name}" while renaming sprite.')

            if 'sprites' not in struct:
                continue

            for sprite in struct['sprites']:
                if 'name' not in sprite:
                    continue
                if sprite['name'] == old_name:
                    sprite['name'] = new_name

            content = json.dumps(struct)

            outs = s.file.replace(None, False, Gio.FileCreateFlags.NONE)
            ok, bytes_written = outs.write_all(content.encode('utf-8'))

    def remove_sprite(self, sprite: lib.SceneSprite):
        if sprite not in self.scene_sprites:
            raise Exception('Sprite is not present in the scene')
        self.scene_sprites.remove(sprite)
        self._content_changed()

    def _content_changed(self):
        self._batch_changes_changed = True
        if self.props.batch_changes:
            return
        self.emit('content-changed')

    def _on_sprite_texture_notify(self, obj, prop):
        self.emit('sprite-texture-changed')

    def _on_scene_deleted(self, scene):
        self._scan_if_not_monitored()

    def _on_scene_renamed(self, scene):
        self._scan_if_not_monitored()

    def _on_dir_monitor_changed(self, monitor, file, other_file, event_type):
        if event_type == Gio.FileMonitorEvent.CHANGES_DONE_HINT:
            return
        if other_file is None:
            f1ext = os.path.splitext(file.get_basename())[1]
            if f1ext not in lib.IMAGE_EXTENSIONS and f1ext != '.tcs':
                return
        self.scan_project()
