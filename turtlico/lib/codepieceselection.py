# codepieceselection.py
#
# Copyright 2021 saytamkenorh
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

from gi.repository import GObject

import turtlico.lib as lib


class CodePieceSelection(GObject.Object):
    @GObject.Property
    def start_x(self):
        return self._start_x

    @GObject.Property
    def start_y(self):
        return self._start_y

    @GObject.Property
    def end_x(self):
        return self._end_x

    @GObject.Property
    def end_y(self):
        return self._end_y

    def __init__(self, start_x, start_y, end_x, end_y):
        super().__init__()

        assert start_x >= 0
        assert start_y >= 0
        assert end_x >= 0
        assert end_y >= 0

        if end_y < start_y or (start_y == end_y and start_x > end_x):
            start_x, end_x = end_x, start_x
            start_y, end_y = end_y, start_y

        self._start_x = int(start_x)
        self._start_y = int(start_y)
        self._end_x = int(end_x)
        self._end_y = int(end_y)

    def __str__(self) -> str:
        return f'CodePieceSelection(start_x={self.props.start_x}, start_y={self.props.start_y}, end_x={self.props.end_x}, end_y={self.props.end_y})'  # noqa: E501

    def validate(self, code: lib.CodePiece):
        self._validate_y(self.props.start_y, code, 'start_y')
        self._validate_x(
            self.props.start_x, self.props.start_y, code, 'start_x')

        self._validate_y(self.props.end_y, code, 'end_y')
        self._validate_x(
            self.props.end_x, self.props.end_y, code, 'end_x')

    def _validate_y(self, y: int, code: lib.CodePiece, p: str):
        if y < 0:
            raise Exception(f'Parameter {p} must not be negative')
        if y >= len(code):
            raise Exception(
                f'Parameter {p} must be smaller than the number of lines')

    def _validate_x(self, x: int, y: int, code: lib.CodePiece, p: str):
        if x < 0:
            raise Exception(f'Parameter {p} must not be negative')
        if x >= len(code[y]):
            raise Exception(
                f'Parameter {p} must be smaller than the number of lines')

    def contains(self, x: int, y: int) -> bool:
        if y < self.props.start_y:
            return False
        if y > self.props.end_y:
            return False
        if y == self.props.start_y and x < self.props.start_x:
            return False
        if y == self.props.end_y and x > self.props.end_x:
            return False
        return True
