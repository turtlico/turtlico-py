# flake8: noqa
from turtlico.lib import Plugin, CommandCategory
from turtlico.lib import CommandDefinition, CommandType, CommandModule, CommandEvent
from turtlico.lib.icon import icon, text
from turtlico.locale import _

def get_plugin():
    p = Plugin('Gpiozero (Raspberry Pi)', doc_url='ref_rpi.html')
    p.categories = [
        CommandCategory(p, icon('raspberrypi.svg'), [
            CommandDefinition('rpi_led', text('LED'), _('Connect LED'), CommandType.METHOD, 'LED', ''),
            CommandDefinition('rpi_rgb', text('RGB'), _('Connect RGB LED'), CommandType.METHOD, 'RGBLED', ''),
            CommandDefinition('rpi_btn', text('BTN'), _('Connect Button (Digital Input Device)'), CommandType.METHOD, 'Button', ''),
            CommandDefinition('rpi_pwm', text('PWM'), _('Connect PWM Output Device'), CommandType.METHOD, 'PWMOutputDevice', ''),
            CommandDefinition('rpi_out', text('OUT'), _('Connect Digital Output Device'), CommandType.METHOD, 'DigitalOutputDevice', ''),
            CommandDefinition('rpi_on', icon('rpion.svg'), _('Turn on device method'), CommandType.METHOD, 'on', ''),
            CommandDefinition('rpi_off', icon('rpioff.svg'), _('Turn off device method'), CommandType.METHOD, 'off', ''),
            CommandDefinition('rpi_value', text('🔧'), _('Value property (input and output)'), CommandType.CODE_SNIPPET, 'value', ''),
            CommandDefinition('rpi_color', icon('rpicolor.svg'), _('Converts color to format suitable for use with gpiozero'), CommandType.METHOD, 'tcf_rpi_color', ''),
            CommandDefinition('rpi_event_on', icon('rpieventon.svg'), _('Connect a function to check when the device is turned on'), CommandType.METHOD, 'tcf_rpi_event_on', ''),
            CommandDefinition('rpi_event_off', icon('rpieventoff.svg'), _('Connect a function to check when the device is turned off'), CommandType.METHOD, 'tcf_rpi_event_off', ''),
        ])
    ]
    p.modules = {
        'rpi': CommandModule(
            deps=(),
            code="""from gpiozero import *"""
        ),
        'tcf_rpi_event_on': CommandModule(
            deps=(),
            code="""def tcf_rpi_event_on(obj, function):
	if isinstance(obj, Button):
		obj.when_pressed = function"""
        ),
        'tcf_rpi_event_off': CommandModule(
            deps=(),
            code="""def tcf_rpi_event_off(obj, function):
	if isinstance(obj, Button):
		obj.when_released = function"""
        ),
        'tcf_rpi_color': CommandModule(
            deps=(),
            code="""def tcf_rpi_color(color):
	return (color[0] / 255, color[1] / 255, color[2] / 255)"""
        ),
    }
    p.events = [
        CommandEvent(
            name=_('Gpiozero device turned on'),
            handler='',
            connector='rpi_event_on;(;obj,device;sep;obj,{};),;',
            params=''
        ),
        CommandEvent(
            name=_('Gpiozero device turned off'),
            handler='',
            connector='rpi_event_off;(;obj,device;sep;obj,{};),;',
            params=''
        ),
    ]
    return p
