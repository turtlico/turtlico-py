# Copyright (C) 2021 saytamkenorh
#
# This file is part of Turtlico.
#
# Turtlico is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turtlico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Turtlico.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from gi.repository import Gtk


@Gtk.Template(resource_path='/io/gitlab/Turtlico/ui/appnotifications.ui')
class AppNotifications(Gtk.Box):
    __gtype_name__ = 'TurtlicoAppNotifications'

    def __init__(self) -> None:
        super().__init__()

    def add(self, notif: Gtk.InfoBar):
        """Add a new notification widget to self.
        Automatically handles the CLOSE reponse.

        Args:
            notif (Gtk.InfoBar): The widget
        """
        def callback(widget, resp):
            if resp == Gtk.ResponseType.CLOSE:
                notif.unparent()
        notif.connect('response', callback)

        self.append(notif)

    def add_simple(
            self, text: str, msgtype: Gtk.MessageType = Gtk.MessageType.INFO,
            closebtn=True):
        notif = Gtk.InfoBar.new()
        notif.props.show_close_button = closebtn
        notif.props.message_type = msgtype

        label = Gtk.Label.new(text)
        notif.add_child(label)

        self.add(notif)

    def clear(self):
        child = self.get_first_child()
        while child is not None:
            self.remove(child)
            child = child.get_next_sibling()
