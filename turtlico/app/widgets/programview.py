# Copyright (C) 2021 saytamkenorh
#
# This file is part of Turtlico.
#
# Turtlico is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turtlico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Turtlico.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations
import math
import threading
from collections import namedtuple
from typing import Union, Tuple

from gi.repository import GLib, GObject, Gio, Gtk, Gdk, Graphene

import turtlico.app.widgets as widgets
import turtlico.lib as lib
import turtlico.lib.icon as icon
import turtlico.utils as utils

SelectionStart = namedtuple('SelectionStart', ['mouse_x', 'mouse_y', 'x', 'y'])


class ProgramView(Gtk.Widget, Gtk.Scrollable):
    __gtype_name__ = 'TurtlicoProgramView'

    _selection: lib.CodePieceSelection

    @GObject.Property(type=lib.CodePieceSelection)
    def selection(self):
        return self._selection

    @selection.setter
    def selection(self, value: lib.CodePieceSelection):
        has_selection = value is not None
        if has_selection:
            value.validate(self._codebuffer.lines)
        self._selection = value
        self._copy_action.props.enabled = has_selection
        self._cut_action.props.enabled = has_selection
        self.queue_draw()

    icon_hovered = Union[Tuple[int, int, lib.Command], None]
    auto_indent = GObject.Property(type=bool, default=True)
    compiler = GObject.Property(type=lib.Compiler, default=None)
    preview_code: str
    preview_debug_info: dict[tuple[int, int], int]  # Icon pos - Python line

    _codebuffer: lib.CodeBuffer
    _codebuffer_code_changed_id: int
    _colors: icon.CommandColorScheme

    _drag_source: Gtk.DragSource
    _drag_source_copy: Gtk.DragSource
    _drop_target: Gtk.DropTarget
    _drag_selection: Gtk.GestureDrag
    _motion_controller: Gtk.EventControllerMotion
    _single_controller: Gtk.GestureSingle
    _long_press_controller: Gtk.GestureLongPress
    _hadjustment: Gtk.Adjustment
    _hscroll_policy: Gtk.ScrollablePolicy
    _shortcut_controller: Gtk.ShortcutController
    _vadjustment: Gtk.Adjustment
    _vscroll_policy: Gtk.ScrollablePolicy
    _menu: widgets.ProgramViewMenu

    _copy_action: Gio.SimpleAction
    _cut_action: Gio.SimpleAction
    _paste_action: Gio.SimpleAction

    _drag_selection_start: SelectionStart
    _last_ptr_pos: Union[(int, int), None]
    _preview_cancellable: Gio.Cancellable

    @GObject.Property(type=Gtk.ScrollablePolicy,
                      default=Gtk.ScrollablePolicy.NATURAL)
    def vscroll_policy(self):
        return self._vscroll_policy

    @vscroll_policy.setter
    def vscroll_policy(self, value):
        self._vscroll_policy = value

    @GObject.Property(type=Gtk.ScrollablePolicy,
                      default=Gtk.ScrollablePolicy.NATURAL)
    def hscroll_policy(self):
        return self._hscroll_policy

    @GObject.Property(type=Gtk.Adjustment)
    def hadjustment(self):
        return self._hadjustment

    @hadjustment.setter
    def hadjustment(self, value):
        self._hadjustment = value
        self._hadjustment.step_increment = icon.WIDTH
        self._hadjustment.connect('value-changed',
                                  self._on_adjustment_value_changed)
        self._update_hadjustment()

    @GObject.Property(type=Gtk.Adjustment)
    def vadjustment(self):
        return self._vadjustment

    @vadjustment.setter
    def vadjustment(self, value):
        self._vadjustment = value
        self._vadjustment.step_increment = icon.HEIGHT
        self._vadjustment.connect('value-changed',
                                  self._on_adjustment_value_changed)
        self._update_vadjustment()

    @GObject.Signal
    def icon_hover(self):
        pass

    @GObject.Signal
    def preview_update(self):
        """Emmited when new preview code is available
        """
        self.queue_draw()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        # Fields
        self._codebuffer = None
        self._colors = None
        self._drag_selection_start = None
        self._last_ptr_pos = None
        self._preview_cancellable = None
        self.preview_code = None
        self.preview_debug_info = None

        # Actions
        action_group = Gio.SimpleActionGroup.new()

        self._copy_action = utils.group_add_action(
            action_group, 'edit.copy', self._on_copy)
        self._cut_action = utils.group_add_action(
            action_group, 'edit.cut', self._on_cut)
        self._paste_action = utils.group_add_action(
            action_group, 'edit.paste', self._on_paste)

        self.insert_action_group('programview', action_group)

        # Properties
        self.props.has_tooltip = True
        self.props.focusable = True
        self.props.selection = None

        # Widgets
        self._menu = widgets.ProgramViewMenu()
        self._menu.set_parent(self)
        self._menu.props.programview = self

        # Controllers
        self._drag_source = Gtk.DragSource.new()
        self._drag_source.props.actions = Gdk.DragAction.MOVE
        self._drag_source.connect('prepare', self._on_drag_prepare)
        self._drag_source.connect('drag_end', self._on_drag_end)
        self.add_controller(self._drag_source)

        self._drag_source_copy = Gtk.DragSource.new()
        self._drag_source_copy.props.actions = Gdk.DragAction.COPY
        self._drag_source_copy.props.button = Gdk.BUTTON_SECONDARY
        self._drag_source_copy.connect('prepare', self._on_drag_prepare)
        self._drag_source_copy.connect('drag_end', self._on_drag_end)
        self.add_controller(self._drag_source_copy)

        self._drop_target = Gtk.DropTarget.new(
            lib.CodePieceDrop, Gdk.DragAction.COPY | Gdk.DragAction.MOVE)
        self._drop_target.connect('drop', self._on_drop_target_drop)
        self.add_controller(self._drop_target)

        self._drag_selection = Gtk.GestureDrag.new()
        self._drag_selection.connect('drag-begin', self._on_selection_begin)
        self._drag_selection.connect('drag-update', self._on_selection_update)
        self._drag_selection.connect('drag-end', self._on_selection_end)
        self.add_controller(self._drag_selection)

        self._motion_controller = Gtk.EventControllerMotion()
        self._motion_controller.connect('enter', self._on_ptr_enter)
        self._motion_controller.connect('motion', self._on_ptr_motion)
        self._motion_controller.connect('leave', self._on_ptr_leave)
        self.add_controller(self._motion_controller)

        self._single_controller = Gtk.GestureSingle()
        self._single_controller.props.button = 0
        self._single_controller.connect('end', self._on_single_end)
        self.add_controller(self._single_controller)

        self._long_press_controller = Gtk.GestureLongPress()
        self._long_press_controller.connect('pressed', self._on_long_pressed)
        self.add_controller(self._long_press_controller)

        # Shortcuts
        self._shortcut_controller = Gtk.ShortcutController()
        self._shortcut_controller.add_shortcut(
            utils.new_shortcut('Delete', self._on_delete_shortcut))
        self._shortcut_controller.add_shortcut(
            utils.new_shortcut('F2', self._on_edit_shortcut))
        self._shortcut_controller.add_shortcut(
            utils.new_action_shortcut('<Ctrl>C', 'programview.edit.copy'))
        self._shortcut_controller.add_shortcut(
            utils.new_action_shortcut('<Ctrl>X', 'programview.edit.cut'))
        self._shortcut_controller.add_shortcut(
            utils.new_action_shortcut('<Ctrl>V', 'programview.edit.paste'))
        self._shortcut_controller.add_shortcut(
            utils.new_shortcut('<Ctrl>Z', self._on_undo_shortcut))
        self._shortcut_controller.add_shortcut(
            utils.new_shortcut('<Ctrl>Y', self._on_redo_shortcut))
        self.add_controller(self._shortcut_controller)

        self._update_adjustments()

    def do_snapshot(self, snapshot: Gtk.Snapshot):
        if not self._colors:
            return
        # Scroll
        tx = -int(self.props.hadjustment.props.value)
        ty = -int(self.props.vadjustment.props.value)

        area = Graphene.Rect().init(0, 0, self.get_width(), self.get_height())

        snapshot.append_color(
            self._colors[icon.CommandColor.INDENTATION][0], area)
        if not self._codebuffer:
            return

        # Content
        snapshot.push_clip(area)
        end_x = math.ceil((self.props.hadjustment.props.value
                          + self.get_width()) / icon.WIDTH)
        start_y = int(self.props.vadjustment.props.value / icon.HEIGHT)
        end_y = math.ceil((self.props.vadjustment.props.value
                          + self.get_height()) / icon.HEIGHT)
        icon.append_block_to_snapshot(self._codebuffer.lines,
                                      snapshot, tx, ty,
                                      self, self._colors, self._codebuffer,
                                      end_x, start_y, end_y,
                                      self.preview_debug_info)
        # Selection
        if self.props.selection:
            start_y = self.props.selection.start_y
            end_y = self.props.selection.end_y
            for y in range(start_y, end_y + 1):
                start_x = (
                    0 if y > start_y
                    else self._line_x_to_render_x(
                        self.props.selection.start_x, y)
                )
                end_x = self._line_x_to_render_x(
                    len(self._codebuffer.lines[y]) - 1 if y < end_y
                    else self.props.selection.end_x, y, True)
                selection_rect = Graphene.Rect().init(
                    tx + start_x * icon.WIDTH, ty + y * icon.HEIGHT,
                    (end_x - start_x + 1) * icon.WIDTH,
                    icon.HEIGHT
                )
                snapshot.append_color(
                    self._colors[icon.CommandColor.OVERLAY_SELECTION][0],
                    selection_rect)

        snapshot.pop()
        snapshot.render_focus(self.get_style_context(), 0, 0,
                              self.get_width(), self.get_height())

    def do_size_allocate(self, width: int, height: int, baseline: int):
        self._update_adjustments()

    def do_measure(self, orientation, for_size):
        if orientation == Gtk.Orientation.HORIZONTAL:
            width = self.props.hadjustment.props.upper
            return (width, width, -1, -1)
        else:
            height = self.props.vadjustment.props.upper
            return (height, height, -1, -1)

    def do_query_tooltip(self,
                         x, y,
                         keyboard_tooltip: bool,
                         tooltip: Gtk.Tooltip) -> bool:
        cmd = self.get_command_at(x, y)[0]
        if cmd is None or not cmd.data:
            return False
        tooltip.set_text(cmd.data)
        return True

    def _update_adjustments(self):
        self._update_hadjustment()
        self._update_vadjustment()

    def _update_hadjustment(self):
        if not self.props.hadjustment:
            self.props.hadjustment = Gtk.Adjustment.new(0, 0, 0, 0, 0, 0)
        width = 3
        if self._codebuffer and len(self._codebuffer.lines) > 0:
            width += (max(
                [self._line_x_to_render_x(len(line), y)
                 for y, line in enumerate(self._codebuffer.lines)]))
        width *= icon.WIDTH

        self.props.hadjustment.props.lower = 0
        self.props.hadjustment.props.upper = width
        self.props.hadjustment.props.page_size = min(width, self.get_width())
        self.props.hadjustment.props.value = min(
            self.props.hadjustment.props.value,
            width - self.props.hadjustment.props.page_size
        )

    def _update_vadjustment(self):
        if not self.props.vadjustment:
            self.props.vadjustment = Gtk.Adjustment.new(0, 0, 0, 0, 0, 0)
        height = 0
        if self._codebuffer:
            if not self._codebuffer.props.single_line:
                height += 3
            height += len(self._codebuffer.lines)
        height = max(1, height)

        height *= icon.HEIGHT

        self.props.vadjustment.props.lower = 0
        self.props.vadjustment.props.upper = height
        self.props.vadjustment.props.page_size = min(height, self.get_height())
        self.props.vadjustment.props.value = min(
            self.props.vadjustment.props.value,
            height - self.props.vadjustment.props.page_size
        )

    def _on_adjustment_value_changed(self, adjustment):
        self.queue_draw()

    def do_get_request_mode(self):
        return Gtk.SizeRequestMode.CONSTANT_SIZE

    def get_command_at(self, x: float, y: float
                       ) -> tuple[lib.Command, int, int]:
        """Return Command at widget position x,y

        Args:
            x (float): X coordinate in pixels
            y (float): Y coordinate in pixels

        Returns:
            tuple[lib.Command, int, int]:
                The Command (or None) and its coords
        """
        x, y = self._get_program_coords(x, y)
        if y >= len(self._codebuffer.lines) or y < 0:
            return (None, x, y)
        if x >= len(self._codebuffer.lines[y]) or x < 0:
            return (None, x, y)
        return (self._codebuffer.lines[y][x], x, y)

    def get_coords_at(self, x: int, y: int, x_end: bool) -> tuple[int, int]:
        tx = -int(self.props.hadjustment.props.value)
        ty = -int(self.props.vadjustment.props.value)
        x = self._line_x_to_render_x(x, y, x_end) * icon.WIDTH + tx
        y = y * icon.HEIGHT + ty
        return (x, y)

    def drop_coords_to_program(self, x: float, y: float) -> tuple[int, int]:
        """Returns a position in program
            where should be inserted icons that were dropped at x,y

        Args:
            x (float): X coordinate in pixels
            y (float): Y coordinate in pixels

        Returns:
            (int, int): Line number and column number
        """
        x, y = self._get_program_coords(x, y)
        lineslen = len(self._codebuffer.lines)

        y = min(y, lineslen)
        if y == -1:
            return (0, 0)
        if y >= lineslen:
            return (0, y)

        if self._codebuffer.props.single_line:
            x = min(x, len(self._codebuffer.lines[y]))
        else:
            # Drops just before the enter icon
            x = min(x, len(self._codebuffer.lines[y]) - 1)
        return (x, y)

    def _get_program_coords(self, x: float, y: float) -> tuple[int, int]:
        """Converts mouse coordinates to icon column and line

        Args:
            x (float): X coordinate in pixels
            y (float): Y coordinate in pixels

        Returns:
            (int, int): Icon column and line
        """
        x += self.props.hadjustment.props.value
        y += self.props.vadjustment.props.value
        x = math.floor(x / icon.WIDTH)
        y = math.floor(y / icon.HEIGHT)

        if y < len(self._codebuffer.lines) and y >= 0:
            linelen = len(self._codebuffer.lines[y]) - 1
            program_x = 0
            render_x = -1
            while True:
                if program_x < linelen:
                    c = self._codebuffer.lines[y][program_x]
                    w = icon.calc_icon_width(c, self)
                    render_x += w
                else:
                    render_x += 1
                if render_x >= x:
                    break
                program_x += 1
            x = program_x
        return (x, y)

    def _line_x_to_render_x(self,
                            x: float, y: float,
                            fully_include_end: bool = False) -> int:
        if y >= len(self._codebuffer.lines):
            return x
        end = min(x, len(self._codebuffer.lines[y]) - 1)
        if fully_include_end:
            end += 1
        for cx in range(0, end):
            c = self._codebuffer.lines[y][cx]
            w = icon.calc_icon_width(c, self)
            x += w - 1
        return x

    def get_codebuffer(self) -> lib.CodeBuffer:
        return self._codebuffer

    def set_codebuffer(self, codebuffer: lib.CodeBuffer):
        assert isinstance(codebuffer, lib.CodeBuffer)

        if self._codebuffer:
            self._codebuffer.disconnect(self._codebuffer_code_changed_id)

        self._codebuffer = codebuffer
        sid = self._codebuffer.connect(
            'code-changed', self._codebuffer_code_changed)
        self._codebuffer_code_changed_id = sid

        self.request_preview_update()
        self.queue_draw()

    def set_colors(self, colors: icon.CommandColorScheme):
        try:
            icon.validate_color_scheme(colors)
        except Exception as e:
            utils.error(e)
        self._colors = colors
        self.queue_draw()

    def get_selection(self) -> lib.CodePiece:
        return self._codebuffer.get_range(self.props.selection)

    def delete_selection(self):
        self._codebuffer.delete(self.props.selection)
        self.props.selection = None

    def replace_selection(self, new_code: lib.CodePiece):
        assert self.props.selection is not None
        s = self.props.selection
        self._codebuffer.delete(self.props.selection)
        self._codebuffer.insert(
            new_code,
            s.props.start_x, s.props.start_y, self.props.auto_indent)

        if len(new_code) == 0:
            self.props.selection = None
            return

        self.props.selection = lib.CodePieceSelection(
            s.props.start_x, s.props.start_y,
            s.props.start_x + (
                len(new_code[-1]) - 1 if len(new_code) > 0 else 0),
            s.props.start_y + len(new_code) - 1
        )

    def scroll_to_selection(self):
        if self.props.selection is None:
            x = 0
            y = 0
        else:
            x = self.props.selection.props.start_x * icon.WIDTH
            y = self.props.selection.props.start_y * icon.HEIGHT

        hadj = self.props.hadjustment
        if (x < hadj.props.value
                or x > hadj.props.value + hadj.props.page_size - icon.WIDTH):
            self.props.hadjustment.props.value = x

        vadj = self.props.vadjustment
        if (y < vadj.props.value
                or y > vadj.props.value + vadj.props.page_size - icon.HEIGHT):
            self.props.vadjustment.props.value = y

    def paste_commands(self, commands: lib.CodePiece):
        drag = Gdk.Drag.begin(
            self.get_native().get_surface(),
            self.get_display().get_default_seat().get_pointer(),
            lib.get_codepiece_content_provider(commands),
            Gdk.DragAction.COPY,
            0, 0
        )
        paintable = icon.prepare_drag_paintable(commands, self, self._colors)
        Gtk.DragIcon.set_from_paintable(
            drag, *paintable)

    def edit_command(self, x, y):
        assert y < len(self._codebuffer.lines)
        assert x < len(self._codebuffer.lines[y])

        cmd = self._codebuffer.lines[y][x]
        widgets.edit_icon(
            cmd, self._codebuffer.project, utils.get_parent_window(self),
            self._on_edit_command_response, x, y)

    def request_preview_update(self):
        if self.props.compiler is None:
            return
        if self._preview_cancellable is not None:
            self._preview_cancellable.cancel()
        self._preview_cancellable = Gio.Cancellable.new()

        def _update(cancellable):
            code, dinfo = self.compiler.compile(
                self._codebuffer.lines, cancellable)
            dinfo = dict(zip(dinfo.values(), dinfo.keys()))

            GLib.idle_add(self._request_preview_update_done, code, dinfo)
        t = threading.Thread(
            target=_update, args=(self._preview_cancellable,))
        t.daemon = True
        t.start()

    def _request_preview_update_done(self, code, dinfo):
        self.preview_code = code
        self.preview_debug_info = dinfo
        self.emit('preview-update')

    def _on_edit_command_response(self, cmd: lib.Command, x, y):
        if cmd.data is self._codebuffer.lines[y][x].data:
            return
        self._codebuffer.replace_command(cmd, x, y)

    def _on_drop_target_drop(self,
                             dt: Gtk.DropTarget,
                             drop: lib.CodePieceDrop, x, y
                             ):
        if not self._codebuffer:
            return False

        # In local transfers deletes moved icon first before inserting it again
        local_drag = self._drag_source.get_drag()
        if (dt.get_drop().get_drag() == local_drag
                and local_drag.props.selected_action == Gdk.DragAction.MOVE):
            self._codebuffer.record_history_pause = True
            self.delete_selection()
            self._codebuffer.record_history_pause = False

        commands = lib.load_codepiece(
            drop.tcppiece, self._codebuffer.project)
        x, y = self.drop_coords_to_program(x, y)

        self._codebuffer.insert(commands, x, y, self.props.auto_indent)
        return True

    def _codebuffer_code_changed(self, codebuffer):
        self._update_adjustments()
        self.queue_draw()
        self.props.selection = None
        self.request_preview_update()

    def _on_drag_prepare(self,
                         source: Gtk.DragSource, x: float, y: float
                         ) -> Gdk.ContentProvider:
        shift_mask = (source.get_current_event_state()
                      & Gdk.ModifierType.SHIFT_MASK)
        if shift_mask != 0:
            return None
        if self.props.selection:
            commands = self._codebuffer.get_range(self.props.selection)
        else:
            command, cx, cy = self.get_command_at(x, y)
            if not command:
                return None
            commands = [[command]]
            self.props.selection = lib.CodePieceSelection(cx, cy, cx, cy)

        return icon.prepare_drag(source, commands, self, self._colors)

    def _on_drag_end(self,
                     source: Gtk.DragSource,
                     drag: Gdk.Drag, delete_data: bool):
        if delete_data and self.props.selection:
            self.delete_selection()
        self.props.selection = None

    def _on_selection_begin(self,
                            source: Gtk.GestureDrag,
                            start_x: float, start_y: float):
        cmd, x, y = self.get_command_at(start_x, start_y)
        shift_mask = (source.get_current_event_state()
                      & Gdk.ModifierType.SHIFT_MASK)
        if cmd is None:
            self.props.selection = None
            return
        if shift_mask == 0:
            self._drag_selection_start = None
            return
        self.props.selection = lib.CodePieceSelection(x, y, x, y)
        self._drag_selection_start = SelectionStart(
            start_x, start_y, x, y
        )

    def _on_selection_update(self,
                             source: Gtk.GestureDrag,
                             offset_x: float, offset_y: float):
        if ((self.selection is None)
                or (self._drag_selection_start is None)):
            return
        sx = self._drag_selection_start.x
        sy = self._drag_selection_start.y
        ex, ey = self._get_program_coords(
            self._drag_selection_start.mouse_x + offset_x,
            self._drag_selection_start.mouse_y + offset_y
        )

        ey = min(len(self._codebuffer.lines) - 1, ey)
        if ey < 0:
            return
        ex = min(len(self._codebuffer.lines[ey]) - 1, ex)
        if ex < 0:
            return

        self.props.selection = lib.CodePieceSelection(sx, sy, ex, ey)

    def _on_selection_end(self,
                          source: Gtk.GestureDrag,
                          offset_x: float, offset_y: float):
        self._drag_selection_start = None

    def _on_delete_shortcut(self, widget, args):
        if self.props.selection is not None:
            self.delete_selection()
            return
        if self._last_ptr_pos is None:
            return
        x, y = self._last_ptr_pos
        cmd, cx, cy = self.get_command_at(x, y)
        if cmd is None:
            return
        self.props.selection = lib.CodePieceSelection(cx, cy, cx, cy)
        self.delete_selection()

    def _on_ptr_enter(self, widget, x, y):
        self._on_ptr_motion(widget, x, y)

    def _on_ptr_motion(self, widget, x, y):
        cmd, cx, cy = self.get_command_at(x, y)
        if cmd is None:
            self.icon_hovered = None
            self.emit('icon-hover')
        else:
            self.icon_hovered = (cx, cy, cmd)
            self.emit('icon-hover')
        self._last_ptr_pos = (x, y)
        self.grab_focus()

    def _on_ptr_leave(self, widget):
        self.icon_hovered = None
        self.emit('icon-hover')
        self._last_ptr_pos = None

    def _on_single_end(self, ctl, sequence):
        self.grab_focus()
        if ctl.get_current_button() == Gdk.BUTTON_SECONDARY:
            if self._last_ptr_pos is not None:
                x, y = self._last_ptr_pos
                self._menu.open_at_icon(x, y)

    def _on_long_pressed(self, ctl, x, y):
        self._menu.open_at_icon(x, y)

    def _on_edit_shortcut(self, action, args):
        if self._last_ptr_pos is None:
            return
        x, y = self._last_ptr_pos
        cmd, cx, cy = self.get_command_at(x, y)
        if cmd is None:
            return
        self.edit_command(cx, cy)

    def _on_copy(self, action, args):
        if self.props.selection is None:
            return
        commands = self.get_selection()
        cp = lib.get_codepiece_content_provider(commands)
        clipboard = self.get_display().get_clipboard()
        clipboard.set_content(cp)

    def _on_cut(self, action, args):
        if self.props.selection is None:
            return
        self._on_copy(action, args)
        self.delete_selection()

    def _on_paste(self, action, args):
        tcppiece = lib.get_tcppiece_from_clipboard(self)
        if tcppiece is None:
            return
        commands = lib.load_codepiece(tcppiece, self._codebuffer.project)
        self.paste_commands(commands)

    def _on_undo_shortcut(self, widget, args):
        self._codebuffer.undo()

    def _on_redo_shortcut(self, widget, args):
        self._codebuffer.redo()


GObject.type_register(ProgramView)
