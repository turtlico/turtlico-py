=========
Base docs
=========
.. container:: cmd
   :name: nl

   **New line (enter)**

.. container:: cmd
   :name: tab

   **Tab**

   Tabs are used to create blocks of commands. For example it is used in order to specify what commands should happen if a condition is true or what commands should be executed n-times.

   .. image:: figures/tabs.png
      :align: center

.. container:: cmd
   :name: c23

   **Comment**

   Comments are editable labels.
   Their purpose is to explain parts of the program.

.. container:: cmd
   :name: sep

   **Argument separator (Comma)**

   Comma is used to separate arguments in function calls, list items etc.

.. container:: cmd
   :name: c28

   **Left parenthesis**

   Parenthesis are used to specify parameters of commands.
   An example usage is "command(parameter, parameter)".

.. container:: cmd
   :name: c29

   **Right parenthesis**

   See `here <#c29>`_.

.. container:: cmd
   :name: if

   **If statement**

   If statements are used to create blocks of commands that are executed only if a specified condition is true.
.. container:: cmd
   :name: else

   **Else**

   Runs the following block of commands if the previous condition was false.

   You can add If icon after Else to create an Else if statement that runs the block of commands if the previous condition was false
   and the condition following after If was true.
.. container:: cmd
   :name: c3a

   **Begin block of commands**

   Marks a block of commands for a statement like conditions or cycles.

   .. image:: figures/tabs.png
      :align: center

.. container:: cmd
   :name: c26

   **and**

   True if the preceding condition and the following condition are true.
.. container:: cmd
   :name: c7cc7c

   **or**

   True if at least one of surrounding conditions is true.
.. container:: cmd
   :name: c21

   **negation**

   Inverts the following statement. It's true if the following statement is false and vice versa.
.. container:: cmd
   :name: c3dc3d

   **equals**

   Checks whether surrounding statements are equal.
.. container:: cmd
   :name: c21c3d

   **not equals**

   Checks whether surrounding statements are not equal.
.. container:: cmd
   :name: c3c

   **is less than**

   Compares two surrounding numbers.
   Can be combined with `equals <#c3dc3d>`__ in order to create less or equals.

.. container:: cmd
   :name: c3e

   **is greater than**

   Compares two surrounding numbers.
   Can be combined with `equals <#c3dc3d>`__ in order to create greater or equals.

.. container:: cmd
   :name: rep

   **Repeat block of commands**

   This is used to repeat block of commands n times.
   Place a ``NUMBER`` after this command. If no number is specified the cycle will repeat infinitely.

   See `Begin fill <./ref_turtle.html#bf>`__ for an example.

.. container:: cmd
   :name: for

   **For loop**

   For loop executes a block of commands for every item in an iterable object (list etc.).
   List values are assigned to a variable that you can use then.

   .. image:: figures/for-loop.png
      :align: center

.. container:: cmd
   :name: in

   **is item in an list? / for items in a colletion**

   Checks wheter item is in a list.

   It is also used to create `for loops <#for>`_.

.. container:: cmd
   :name: while

   **Repeat while the condition is true**

   Checks whether the following condition is true.
   Then runs the following block of commands and does so until the condition if false.

   .. figure:: figures/while-loop.png
      :align: center

   Create a "wall" up to X coordinate 200.

.. container:: cmd
   :name: b

   **Break out of a loop**

   Stops the loop the command is directly placed in.
   The program execution continues at command after the cycle.
.. container:: cmd
   :name: c

   **Continue with the next iteration of a loop**

   Skips the rest of the commands block of the loop and continues with the next iteration
   (or exits the cycle if current iteration was the last one).
.. container:: cmd
   :name: def

   **Define a function**

   Functions are used to deduplicate blocks of code.
   The function definition has to be executed before using the function.

   You can also use the functions dialog to create functions and connect events to them with less effort.
   There you can also create function calls (`object <#obj>`__ with parenthesis).

   Functions can also return values just like normal commands.

   .. figure:: figures/function.png
      :align: left

   Creates an example function called "sum" that returns sum of two numbers.

.. container:: cmd
   :name: r

   **Return a value**

   See `functions <#def>`__.
.. container:: cmd
   :name: c5b

   **Left square bracket**

   Square brackets are used create lists and to access list elements by index.

   Example usage of list-related commands:

   .. image:: figures/lists.png
      :align: center

.. container:: cmd
   :name: c5d

   **Right square bracket**

   See `Left square bracket <#c5b>`__.
.. container:: cmd
   :name: len

   **Length of (string, list etc)**

   Gets the length of the object.
   For example, number of items in a collection or count of characters in a string.

   *Parameters*

   * ``OBJECT`` The object whose length is to be obtained
.. container:: cmd
   :name: del

   **Delete an object**

   Deletes a varibale.

   May be also used to remove items from list.
   For more information about lists see `this <#c5b>`__.

   *Parameters*

   * ``OBJECT`` The variable
.. container:: cmd
   :name: apnd

   **Append an item to the list**

   For more information about lists see `this <#c5b>`__.

   *Parameters*

   * ``OBJECT`` The item
.. container:: cmd
   :name: ins

   **Insert an item to the list**

   For more information about lists see `this <#c5b>`__.

   *Parameters*

   * ``NUMBER`` Index
   * ``OBJECT`` The item
.. container:: cmd
   :name: clr

   **Clear the list**

   Removes all items from the list.

   For more information about lists see `this <#c5b>`__.

.. container:: cmd
   :name: index

   **Find index of the first occurence of an item in a list or string**

   Returns -1 if no occurence was found.

   For more information about lists see `this <#c5b>`__.

   *Parameters*

   * ``LIST`` List to find items in.
   * ``OBJECT`` Item to search for.
   * ``NUMBER`` Search start index. (default: 0)
   * ``NUMBER`` Direction of search. (direction: 1)
.. container:: cmd
   :name: slo

   **Convert a string to lower case**

   Makes the whole string to contain only lower case letters.

   *Parameters*

   * ``STRING`` The string
.. container:: cmd
   :name: sup

   **Convert a string to upper case**

   Makes the whole string to contain only upper case letters.

   *Parameters*

   * ``STRING`` The string
.. container:: cmd
   :name: sspl

   **Split the string**

   Splits the string using a specified separator. Returns a list.

   *Parameters*

   * ``STRING`` The separator
.. container:: cmd
   :name: srep

   **Replace a phrase with another phrase in the string**

   Returns the edited string.

   *Parameters*

   * ``STRING`` Phrase to be replaced
   * ``STRING`` String to replace the phrase with
.. container:: cmd
   :name: global

   *Define global variable*

   Forces specified object to be used as a global variable.
   Global variables have persistent values all over the program.
   For example if you change value of a global varibale in a function it will affect all occurences of the variable in the code.

   If you would assign a value to a global varibale without marking it as global at the start of the function or program the program will only create a new local variable
   that will be destroyed after the functions ends.

   *Usage at the start of the program (recommended)*

   If you mark variable as global outside of any commands block it will be marked as global automatically in all following functions.
   This means that if you place this at the start of the program the variable will be accessible from anywhere.

   .. image:: figures/global-varibales.png
      :align: center

   *Usage at start of functions*

   You can also mark variable as global only for specific function. Just place this at the start of the function.

   However if you only read from a global variable you don't have to use this command at all but is is recommended to use it in order to make code easier to read.

.. container:: cmd
   :name: assign

   **assign value**

   See `Object <#obj>`__.

.. container:: cmd
   :name: c2bc3d

   **increase value by**

   Increases value of a variable by specified amount.
   Example:

   .. figure:: figures/increase-value-by.png
      :align: center

   Increases value of variable "n" by 10

.. container:: cmd
   :name: -c3d

   **decrease value by**

   Decreases value of a variable by specified amount.
   Also see `Increase value by <#c2bc3d>`__.

.. container:: cmd
   :name: .

   **Dot (access properties of an object)**

   Some objects (e.g. turtles and lists) have properties like varibales or methods. Dot is used in order to access them.
   To look at an example usage check out `Create new turtle <./ref_turtle.html#newt>`__.

.. container:: cmd
   :name: try

   **Try**

   Expections raised by commands in a command block following a try can be handled by `Except <#exc>`__ without crashing the whole program.
.. container:: cmd
   :name: exc

   **Except**

   Runs the following command block when an exception occurs during the execution of the try.

   If no exception is specified catches all exceptions.
   You can specify an exception in the following form: "Exception type" "as" "variable name".

.. container:: cmd
   :name: rs

   **Raise exception**

   *Parameters*

   * ``EXCEPTION`` The exception
.. container:: cmd
   :name: c2b

   **plus**

.. container:: cmd
   :name: -

   **minus**

.. container:: cmd
   :name: c2a

   **multiply**

.. container:: cmd
   :name: /

   **divide**

.. container:: cmd
   :name: c25

   **modulo**

.. container:: cmd
   :name: tc

   **Type conversion**

   Type conversion is used in order to make value of specified type from another value (e.g. a number from a string).

   Example usage (note that the number 100 is a string):

   .. image:: figures/type-conversion.png
      :align: center

.. container:: cmd
   :name: python

   **Direct Python code**

   This is an editable icon that represents direct Python code.

.. container:: cmd
   :name: as

   **as variable**

   This is used to specify name of variable that contains preceding value.
   For example it is used with For loop.

.. container:: cmd
   :name: rand

   **Random number**

   Returns a random number in range.

   *Parameters*

   * ``NUMBER`` Minimum (default: 0)
   * ``NUMBER`` Maximum (default: 100, excluding)
.. container:: cmd
   :name: range

   **List of numbers in specified range**

   *Parameters*

   * ``NUMBER`` Start (default: 0)
   * ``NUMBER`` End (default: 100, excluding)
.. container:: cmd
   :name: ord

   **Converts a character to an int value**

   Returns an integer represeting the character.
   The result is a Unicode code point or a value of the byte when the argument is an 8-bit string.
   You can also check out the `Unicode table <https://en.wikipedia.org/wiki/List_of_Unicode_characters#Latin_script>`__ to determine int values of various characters.

   *Parameters*

   * ``STRING`` The character. The string must be exactly one character long.
.. container:: cmd
   :name: chr

   **Converts an int value to a character**

   Returns a string representing a character whose Unicode code point is an integer.

   *Parameters*

   * ``NUMBER`` Int value of the character.
.. container:: cmd
   :name: mdlist

   **Create multidimensional list**

   Creates a multidimensional list (multidimensional array/list of lists).
   This can be used for example to store a grid of numbers.

   The list must have at least one dimension. Sizes of dimensions are specified in form of separate parameters.
   The number of parameters specifies the number of dimensions.

   *Parameters*

   * ``OBJECT`` The initial value for all cells.
   * ``NUMBER`` Size of the first dimension of the list.
   * ``NUMBER`` ...
.. container:: cmd
   :name: exit

   **Exit the program**

   *Parameters*

   * ``NUMBER`` Program return code. (default: 0)
.. container:: cmd
   :name: 0

   **East (0°)**

   Direction constant.
.. container:: cmd
   :name: 90

   **North (90°)**

   Direction constant.
.. container:: cmd
   :name: 180

   **West (180°)**

   Direction constant.
.. container:: cmd
   :name: 270

   **South (270°)**

   Direction constant.
.. container:: cmd
   :name: math.pi

   **Pi (3.14)**

   The Pi (π) constant.
.. container:: cmd
   :name: sin

   **Sine**

   Return the sine of x radians.

   *Parameters*

   * ``NUMBER`` Number x
.. container:: cmd
   :name: cos

   **Cosine**

   Return the cosine of x radians.

   *Parameters*

   * ``NUMBER`` Number x
.. container:: cmd
   :name: tan

   **Tangent**

   Return the tangent of x radians.

   *Parameters*

   * ``NUMBER`` Number x
.. container:: cmd
   :name: abs

   **Absolute value**

   Return absolute value of x. Example results 3→3, -3→3.

   *Parameters*

   * ``NUMBER`` Number x
.. container:: cmd
   :name: rad

   **Degrees to radians**

   *Parameters*

   * ``NUMBER`` Angle to be converted
.. container:: cmd
   :name: deg

   **Radians to degrees**

   *Parameters*

   * ``NUMBER`` Angle to be converted
.. container:: cmd
   :name: true

   **True**

   Represents always true condition.

.. container:: cmd
   :name: false

   **False**

   Represents always false condition.

.. container:: cmd
   :name: rnd

   **Round a number**

   *Parameters*

   * ``NUMBER`` "number" The number to be roundend
   * ``NUMBER`` "digits" The number of decimals to use (default: 0)
.. container:: cmd
   :name: floor

   **Floor**

   Return the largest integer not greater than x. E.g. 3.4→3.

   *Parameters*

   * ``NUMBER`` Number x
.. container:: cmd
   :name: ceil

   **Ceil**

   Return the smallest integer not less than x. E.g. 3.4→4.

   *Parameters*

   * ``NUMBER`` Number x
.. container:: cmd
   :name: sqrt

   **Square root**

   Return square root of x. E.g. 16→4.

   *Parameters*

   * ``NUMBER`` Number x

   For getting int powers you can use `** <#c2a>`_:

   .. image:: figures/int-powers.png
      :align: center

.. container:: cmd
   :name: mmin

   **Lowest value**

   Returns the lowest value from specified numbers.
   You can either pass an array or pass the numbers as individual arguments.

   *Parameters*

   * ``LIST[NUMBER]`` The numbers

   *Parameters*

   * ``NUMBER`` First number.
   * ``NUMBER`` Second number.
   * ``NUMBER`` ...

.. container:: cmd
   :name: mmax

   **Highest value**

   Returns the highest value from specified numbers. You can either pass an array or pass the numbers as individual arguments.

   *Parameters*

   * ``LIST[NUMBER]`` The numbers.

   *Parameters*

   ``NUMBER`` First number.
   ``NUMBER`` Second number.
   ``NUMBER`` ...

.. container:: cmd
   :name: color

   **Color (property or editable)**

   This editable icon represents a color. If no color is set then it represents color property.

   If a color is set and the icon is placed standalone into the program it changes turtle pen color.

.. container:: cmd
   :name: font

   **Font (property or editable)**

   This is an editable icon that represents a font description string.

.. container:: cmd
   :name: int

   **Number**

   This editable icon represents a number.

.. container:: cmd
   :name: str

   **String**

   This editable icon represents a string.

.. container:: cmd
   :name: obj

   **Object**

   Objects represent a variable or a function.
   Value of this icon represents the object name.

   **Functions**

   See `functions <#def>`__.

   **Varibales**

   Variables are used to store values like numbers (e.g. game scores) or strings (e.g. user names).
   You have to assert a value to a variable and then you can use it in following code.

   .. figure:: figures/variables-snail.png
      :align: center

      Creates a "snail" using line length variable

.. container:: cmd
   :name: none

   **None**

   None represents a null value or no value at all.
   It is not the same as 0, False or an emtpy string.
   It is a datatype of its own (NoneType).

.. container:: cmd
   :name: key

   **Key**

   Editable icon that represents a key string.

.. container:: cmd
   :name: readlines

   **Read all lines from file**

   Returns an array of strings.

   If file path is None then the functions automatically chooses file name based on the name of the program.

   *Parameters*

   * ``STRING`` File path (default: None)
.. container:: cmd
   :name: writelines

   **Write lines to a file**

   Writes a list of string lines to a file.

   *Parameters*

   * ``LIST[STRING]`` Lines to write
   * ``STRING`` File path (default: None)
.. container:: cmd
   :name: filediag

   **File dialog**

   Opens a file dialog and lets user to choose a file.
   Returns file path as a string.

   *Parameters*

   * ``STRING`` "text" Text to show in the header of the dialog (default: 'Choose a file')
   * ``STRING`` "filter" File filter. Format: '[Filter name] | [File name pattern]' (default: 'All files | \*')
   * ``BOOL`` "save" Activate file save mode
.. container:: cmd
   :name: fileis

   **Check if file exists**

   Returns boolean.

   *Parameters*

   * ``STRING`` File path
.. container:: cmd
   :name: diris

   **Check if directory exists**

   Returns boolean

   *Parameters*

   * ``STRING`` Directory path
.. container:: cmd
   :name: filedirdel

   **Delete file or directory**

   Deletes the specified directory or file.

   *Parameters*

   * ``STRING`` File/directory path
.. container:: cmd
   :name: subfiles

   **Get files in directory**

   Returns a sorted list of subfiles of the specified directory.

   *Parameters*

   * ``STRING`` Directory path
.. container:: cmd
   :name: subdirs

   **Get subdirs in directory**

   Returns a sorted list of subdirs of the specified directory.

   *Parameters*

   * ``STRING`` Directory path
.. container:: cmd
   :name: dirname

   **Get parent directory**

   Returns the parent directory. E.g. for '/dir1/dir2/file' this returns '/dir1/dir2'.

   *Parameters*

   * ``STRING`` Path
.. container:: cmd
   :name: basename

   **Get file name**

   *Parameters*

   * None
.. container:: cmd
   :name: runc

   **Run system command**

   Runs a command in the system shell.

   Please note that commands behave differently on different platforms.

   *Parameters*

   * ``STRING`` The command
.. container:: cmd
   :name: runf

   **Open file in default program**

   Opens a file in the program that is associated with the file type. The path can be also an URL.

   *Parameters*

   * ``STRING`` Path to the file or an URL
.. container:: cmd
   :name: time

   **Get current timestamp**

   Return the time in seconds since the epoch (1 January 1970 00:00:00) as a float.

   *Parameters*

   * None
.. container:: cmd
   :name: timestrf

   **Convert timestamp to string**

   Returns a string that represents the timestamp.

   Format codes:

   +-----------+-------------------------------------------------------+-------------------------------+
   | Directive | Meaning                                               | Example (English (US) locale) |
   +===========+=======================================================+===============================+
   | %a        | Weekday as locale’s                                   |                               |
   |           | abbreviated name.                                     | Sun, Mon, …, Sat              |
   +-----------+-------------------------------------------------------+-------------------------------+
   | %A        | Weekday as locale’s full name.                        | Sunday, Monday, …,            |
   |           |                                                       | Saturday                      |
   +-----------+-------------------------------------------------------+-------------------------------+
   | %w        | Weekday as a decimal number,                          | 0, 1, …, 6                    |
   |           | where 0 is Sunday and 6 is                            |                               |
   |           | Saturday.                                             |                               |
   +-----------+-------------------------------------------------------+-------------------------------+
   | %d        | Day of the month as a                                 | 01, 02, …, 31                 |
   |           | zero-padded decimal number.                           |                               |
   +-----------+-------------------------------------------------------+-------------------------------+
   | %b        | Month as locale’s abbreviated name.                   | Jan, Feb, …, Dec              |
   +-----------+-------------------------------------------------------+-------------------------------+
   | %B        | Month as locale’s full name.                          | January, February,            |
   |           |                                                       | …, December                   |
   +-----------+-------------------------------------------------------+-------------------------------+
   | %m        | Month as a zero-padded decimal number.                | 01, 02, …, 12                 |
   +-----------+-------------------------------------------------------+-------------------------------+
   | %y        | Year without century as a zero-padded decimal number. | 00, 01, …, 99                 |
   +-----------+-------------------------------------------------------+-------------------------------+
   | %Y        | Year with century as a decimal                        | 0001, 0002, …, 2013,          |
   |           | number.                                               | 2014                          |
   +-----------+-------------------------------------------------------+-------------------------------+
   | %H        | Hour (24-hour clock) as a zero-padded decimal number. | 00, 01, …, 23                 |
   +-----------+-------------------------------------------------------+-------------------------------+
   | %I        | Hour (12-hour clock) as a                             | 01, 02, …, 12                 |
   |           | zero-padded decimal number.                           |                               |
   +-----------+-------------------------------------------------------+-------------------------------+
   | %p        | Locale’s equivalent of either                         |                               |
   |           | AM or PM.                                             | AM, PM                        |
   +-----------+-------------------------------------------------------+-------------------------------+
   | %M        | Minute as a zero-padded decimal number.               | 00, 01, …, 59                 |
   +-----------+-------------------------------------------------------+-------------------------------+
   | %S        | Second as a zero-padded                               | 00, 01, …, 59                 |
   |           | decimal number.                                       |                               |
   +-----------+-------------------------------------------------------+-------------------------------+
   | %j        | Day of the year as a                                  | 001, 002, …, 366              |
   |           | zero-padded decimal number.                           |                               |
   +-----------+-------------------------------------------------------+-------------------------------+
   | %U        | Week number of the year                               | 00, 01, …, 53                 |
   |           | (Sunday as the first day of                           |                               |
   |           | the week) as a zero padded                            |                               |
   |           | decimal number. All days in a                         |                               |
   |           | new year preceding the first                          |                               |
   |           | Sunday are considered to be in                        |                               |
   |           | week 0.                                               |                               |
   +-----------+-------------------------------------------------------+-------------------------------+
   | %W        | Week number of the year                               | 00, 01, …, 53                 |
   |           | (Monday as the first day of                           |                               |
   |           | the week) as a decimal number.                        |                               |
   |           | All days in a new year                                |                               |
   |           | preceding the first Monday                            |                               |
   |           | are considered to be in                               |                               |
   |           | week 0.                                               |                               |
   +-----------+-------------------------------------------------------+-------------------------------+
   | %c        | Locale’s appropriate date and time representation.    | Tue Aug 16 21:30:00           |
   |           |                                                       | 2000                          |
   +-----------+-------------------------------------------------------+-------------------------------+
   | %x        | Locale’s appropriate date                             | 08/16/1988                    |
   |           | representation.                                       |                               |
   +-----------+-------------------------------------------------------+-------------------------------+
   | %X        | Locale’s appropriate time                             | 21:30:00                      |
   |           | representation.                                       |                               |
   +-----------+-------------------------------------------------------+-------------------------------+
   | %%        | A literal '%' character.                              | %                             |
   +-----------+-------------------------------------------------------+-------------------------------+

   *Parameters*

   * ``NUMBER`` Timestamp
   * ``STRING`` Format of the output string (default: '%d/%m/%y %H:%M:%S')
.. container:: cmd
   :name: timestrp

   **Convert string to timestamp**

   For list of format codes see `Convert timestamp to string <#timestrf>`__.

   *Parameters*

   * ``NUMBER`` String representing a date/time
   * ``STRING`` Format of the string (default: '%d/%m/%y %H:%M:%S')
