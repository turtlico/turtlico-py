# Copyright (C) 2021 saytamkenorh
#
# This file is part of Turtlico.
#
# Turtlico is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turtlico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Turtlico.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from gi.repository import Gtk, GObject

import turtlico.lib as lib
import turtlico.app.widgets as widgets


@Gtk.Template(resource_path='/io/gitlab/Turtlico/ui/programviewsearch.ui')
class ProgramViewSearch(Gtk.Frame):
    __gtype_name__ = 'TurtlicoProgramViewSearch'

    pw_find: widgets.ProgramView = Gtk.Template.Child()
    pw_replace: widgets.ProgramView = Gtk.Template.Child()
    _find_next_btn: Gtk.Button = Gtk.Template.Child()
    _find_prev_btn: Gtk.Button = Gtk.Template.Child()
    _replace_btn: Gtk.Button = Gtk.Template.Child()
    _replace_all_btn: Gtk.Button = Gtk.Template.Child()

    _cb_find: lib.CodeBuffer
    _cb_replace: lib.CodeBuffer

    _programview: widgets.ProgramView

    @GObject.Property(type=widgets.ProgramView)
    def programview(self) -> widgets.ProgramView:
        return self._programview

    @programview.setter
    def programview(self, value):
        if self._programview is not None:
            raise Exception('Programview can be set only once')

        self._programview = value

        self._cb_find = lib.CodeBuffer(
            self._programview.get_codebuffer().project,
            single_line=True)
        self._cb_find.connect('code-changed', self._on_search_changed)

        self.pw_find.set_codebuffer(self._cb_find)

        self._cb_replace = lib.CodeBuffer(
            self._programview.get_codebuffer().project,
            single_line=True)
        self._cb_replace.connect('code-changed', self._on_search_changed)

        self.pw_replace.set_codebuffer(self._cb_replace)

        self._programview.connect(
            'notify::selection', self._on_programview_selection_notify)
        self._on_programview_selection_notify()
        self._on_search_changed()

    @GObject.Signal
    def close_request(self):
        pass

    def __init__(self):
        self._programview = None
        self._cb_find = None
        self._cb_replace = None
        super().__init__()

    def clear(self):
        self._cb_find.clear()
        self._cb_replace.clear()

    @Gtk.Template.Callback()
    def _on_close_btn_clicked(self, btn):
        self.emit('close-request')

    @Gtk.Template.Callback()
    def _on_find_prev_btn_clicked(self, btn):
        self._search(self._cb_find.lines, True)

    @Gtk.Template.Callback()
    def _on_find_next_btn_clicked(self, btn):
        self._search(self._cb_find.lines, False)

    @Gtk.Template.Callback()
    def _on_replace_btn_clicked(self, btn):
        if len(self._cb_replace.lines) == 0:
            return
        if len(self._cb_replace.lines[0]) == 0:
            return
        self.props.programview.replace_selection(
            self._cb_replace.lines
        )
        self._search(self._cb_find.lines, False)

    @Gtk.Template.Callback()
    def _on_replace_all_btn_clicked(self, btn):
        self.props.programview.props.selection = None
        while self._search(self._cb_find.lines, False, False, False):
            self.props.programview.replace_selection(
                self._cb_replace.lines
            )
        self.props.programview.selection = None

    def _on_programview_selection_notify(self, *args):
        self._replace_btn.props.sensitive = (
            self.props.programview.selection is not None)

    def _on_search_changed(self, *args):
        if self._cb_find is not None and self._cb_replace is not None:
            buf_equals = lib.compare_codepiece(
                self._cb_find.lines,
                self._cb_replace.lines)
        else:
            buf_equals = False

        has_search = len(self._cb_find.lines) > 0

        self._replace_all_btn.props.sensitive = (
            has_search and not buf_equals
        )

        self._find_next_btn.props.sensitive = has_search
        self._find_prev_btn.props.sensitive = has_search

    def _search(
            self, code: lib.CodePiece,
            backwards: bool = False, cycling: bool = True,
            scroll: bool = True) -> bool:
        """Searches for code in the programview.

        Args:
            code (lib.CodePiece): Code to look for.
            backwards (bool, optional): Search backwards. Defaults to False.

        Returns:
            bool: Result found
        """
        buf = self.props.programview.get_codebuffer()

        if len(buf.lines) == 0:
            return False

        if self.props.programview.props.selection is not None:
            if not backwards:
                x = self.props.programview.props.selection.end_x + 1
                y = self.props.programview.props.selection.end_y
            else:
                x = self.props.programview.props.selection.start_x - 1
                y = self.props.programview.props.selection.start_y
        else:
            y = (
                0 if not backwards
                else len(buf.lines) - 1
            )
            x = (
                0 if not backwards
                else len(buf.lines[y]) - 1
            )

        while y >= 0 and y < len(buf.lines):
            while x >= 0 and x < len(buf.lines[y]):
                to_compare = [buf.lines[y][x:(x + len(code[0]))]]
                if lib.compare_codepiece(code, to_compare):
                    s = lib.CodePieceSelection(x, y, x + len(code[0]) - 1, y)
                    self.props.programview.props.selection = s
                    if scroll:
                        self.props.programview.scroll_to_selection()
                    return True
                x += 1 if not backwards else -1
            y += 1 if not backwards else -1
            if y >= 0:
                x = 0 if not backwards else len(buf.lines[y]) - 1

        if self.props.programview.selection is not None and cycling:
            self.props.programview.props.selection = None
            return self._search(code, backwards)

        return False


GObject.type_register(ProgramViewSearch)
