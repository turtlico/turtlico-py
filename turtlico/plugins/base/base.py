# flake8: noqa
from turtlico.lib import Plugin, CommandCategory, LiteralParserResult
from turtlico.lib import CommandDefinition, CommandType, CommandModule, CommandEvent
from turtlico.lib.icon import CommandColor, icon, text
from turtlico.locale import _

name='Base'

def tc(data, toplevel) -> LiteralParserResult:
    return LiteralParserResult(
        f'({data})',
        ()
    )

def raw_data(data, toplevel) -> LiteralParserResult:
    return LiteralParserResult(
        data,
        ()
    )

def string(data, toplevel) -> LiteralParserResult:
    data = data.replace("'", r"\'")
    return LiteralParserResult(
        f"'{data}'",
        ()
    )

def color(data, toplevel) -> LiteralParserResult:
    if not data:
        return LiteralParserResult('color', ())
    if toplevel:
        return LiteralParserResult(
            f'color(({data}))', ()
        )
    else:
        return LiteralParserResult(
            f'({data})', ()
        )

def font(data, toplevel) -> LiteralParserResult:
    if not data:
        return LiteralParserResult('font', ())
    family, size, fonttype, weight = data.split(';')
    return LiteralParserResult(
        f"font = ('{family}', '{size}', '{fonttype}', '{weight}')", ()
    )

def get_plugin():
    p = Plugin(name, doc_url='ref_base.html')
    p.categories = [
        CommandCategory(p, text('⚙'), [
            CommandDefinition('nl', text('⏎'), _('New line (enter)'), CommandType.INTERNAL, color=CommandColor.INDENTATION),
            CommandDefinition('tab', text('·'), _('Tab'), CommandType.INTERNAL, color=CommandColor.INDENTATION),
            CommandDefinition('#', text('#'), _('Comment'), CommandType.INTERNAL, color=CommandColor.COMMENT, data_only=True),
            CommandDefinition('sep', text(','), _('Argument separator (Comma)'), CommandType.CODE_SNIPPET, ', '),
            CommandDefinition('(', text('('), _('Left parenthesis'), CommandType.CODE_SNIPPET, '('),
            CommandDefinition(')', text(')'), _('Right parenthesis'), CommandType.CODE_SNIPPET, ')'),
            CommandDefinition('if', text('❔if'), _('If statement'), CommandType.KEYWORD_WITH_ARGS, 'if', snippet='if,;true,;:,;nl,;tab,;'),
            CommandDefinition('else', text('else'), _('Else'), CommandType.INTERNAL),
            CommandDefinition(':', text(':'), _('Begin block of commands'), CommandType.INTERNAL),
            CommandDefinition('&', text('and'), _('and'), CommandType.CODE_SNIPPET, ' and '),
            CommandDefinition('||', text('or'), _('or'), CommandType.CODE_SNIPPET, ' or '),
            CommandDefinition('!', text('not'), _('negation'), CommandType.CODE_SNIPPET, 'not '),
            CommandDefinition('==', text('=='), _('equals'), CommandType.CODE_SNIPPET, ' == '),
            CommandDefinition('!=', text('!='), _('not equals'), CommandType.CODE_SNIPPET, ' != '),
            CommandDefinition('<', text('<'), _('is less than'), CommandType.CODE_SNIPPET, ' < '),
            CommandDefinition('>', text('>'), _('is greater than'), CommandType.CODE_SNIPPET, ' > '),
            CommandDefinition('rep', icon('rep.svg'), _('Repeat block of commands'), CommandType.INTERNAL, color=CommandColor.CYCLE, snippet='rep;int,2;:;nl;tab;'),
            CommandDefinition('for', text('for'), _('For loop'), CommandType.KEYWORD_WITH_ARGS, 'for', color=CommandColor.CYCLE, snippet='for;obj,n;in;range;(;int,0;sep;int,10;);:;nl;tab;'),
            CommandDefinition('in', text('in'), _('is item in an list? / for items in a colletion'), CommandType.CODE_SNIPPET, ' in '),
            CommandDefinition('while', icon('while.svg'), _('Repeat while the condition is true'), CommandType.KEYWORD_WITH_ARGS, 'while', color=CommandColor.CYCLE, snippet='while;true;:;nl;tab;'),
            CommandDefinition('b', text('🛑'), _('Break out of a loop'), CommandType.KEYWORD, 'break', color=CommandColor.KEYWORD),
            CommandDefinition('c', text('↦'), _('Continue with the next iteration of a loop'), CommandType.KEYWORD, 'continue', color=CommandColor.KEYWORD),
            CommandDefinition('def', text('def'), _('Define a function'), CommandType.INTERNAL, color=CommandColor.KEYWORD, snippet='def;obj,name;(;);:;nl;tab;'),
            CommandDefinition('r', icon('r.svg'), _('Return a value'), CommandType.KEYWORD_WITH_ARGS, 'return', color=CommandColor.KEYWORD),
            CommandDefinition('[', text('['), _('Left square bracket'), CommandType.CODE_SNIPPET, '['),
            CommandDefinition(']', text(']'), _('Right square bracket'), CommandType.CODE_SNIPPET, ']'),
            CommandDefinition('len', text('len'), _('Length of (string, list etc)'), CommandType.METHOD, 'len'),
            CommandDefinition('del', text('del'), _('Delete an object'), CommandType.KEYWORD, 'del', color=CommandColor.KEYWORD),
            CommandDefinition('apnd', icon('apnd.svg'), _('Append an item to the list'), CommandType.METHOD, 'append'),
            CommandDefinition('ins', icon('ins.svg'), _('Insert an item to the list'), CommandType.METHOD, 'insert'),
            CommandDefinition('clr', icon('clr.svg'), _('Clear the list'), CommandType.METHOD, 'clear'),
            CommandDefinition('index', icon('index.svg'), _('Find index of the first occurence of an item in a list or string'), CommandType.METHOD, 'tcf_index'),
            CommandDefinition('slo', text('aa'), _('Convert a string to lower case'), CommandType.METHOD, 'lower'),
            CommandDefinition('sup', text('AB'), _('Convert a string to upper case'), CommandType.METHOD, 'upper'),
            CommandDefinition('sspl', icon('sspl.svg'), _('Split the string'), CommandType.METHOD, 'split', "'\\n'"),
            CommandDefinition('srep', icon('sspl.svg'), _('Replace a phrase with another phrase in the string'), CommandType.METHOD, 'replace'),
            CommandDefinition('global', text('Glob'), _('Define global variable'), CommandType.INTERNAL, color=CommandColor.KEYWORD),
            CommandDefinition('assign', text('='), _('assign value'), CommandType.DIPERATOR, '=', color=CommandColor.KEYWORD),
            CommandDefinition('+=', text('↖'), _('increase value by'), CommandType.DIPERATOR, '+=', color=CommandColor.KEYWORD),
            CommandDefinition('-=', text('↙'), _('decrease value by'), CommandType.DIPERATOR, '-=', color=CommandColor.KEYWORD),
            CommandDefinition('.', text('.'), _('Dot (access properties of an object)'), CommandType.DIPERATOR, '.', color=CommandColor.KEYWORD),
            CommandDefinition('try', text('try:'), _('Try'), CommandType.KEYWORD, 'try:', color=CommandColor.KEYWORD, snippet='try;nl;tab;nl;exc;obj,Exception;as;obj,e;:;nl;tab;'),
            CommandDefinition('exc', icon('exc.svg'), _('Except'), CommandType.KEYWORD_WITH_ARGS, 'except', color=CommandColor.KEYWORD),
            CommandDefinition('rs', icon('rs.svg'), _('Raise exception'), CommandType.KEYWORD_WITH_ARGS, 'raise', color=CommandColor.KEYWORD),
            CommandDefinition('+', text('+'), _('plus'), CommandType.DIPERATOR, '+'),
            CommandDefinition('-', text('-'), _('minus'), CommandType.DIPERATOR, '-'),
            CommandDefinition('*', text('*'), _('multiply'), CommandType.DIPERATOR, '*'),
            CommandDefinition('/', text('/'), _('divide'), CommandType.DIPERATOR, '/'),
            CommandDefinition('%', text('%'), _('modulo'), CommandType.DIPERATOR, '%'),
            CommandDefinition('tc', text('🔄'), _('Type conversion'), CommandType.LITERAL, tc, data_only=False, color=CommandColor.TYPE_CONV),
            CommandDefinition('python', icon('python.svg'), _('Direct Python code'), CommandType.INTERNAL, data_only=False, show_data=False),
            CommandDefinition('as', text('as'), _('as variable'), CommandType.DIPERATOR, 'as', color=CommandColor.KEYWORD),
            CommandDefinition('rand', text('🎲'), _('Random number'), CommandType.METHOD, 'random.randint', '1,100'),
            CommandDefinition('range', icon('range.svg'), _('List of numbers in specified range'), CommandType.METHOD, 'range', '1,10'),
            CommandDefinition('ord', text('a→#'), _('Converts a character to an int value'), CommandType.METHOD, 'ord', ''),
            CommandDefinition('chr', text('#→a'), _('Converts an int value to a character'), CommandType.METHOD, 'chr', ''),
            CommandDefinition('mdlist', icon('mdlist.svg'), _('Create multidimensional list'), CommandType.METHOD, 'tcf_mdlist', ''),
            CommandDefinition('exit', icon('exit.svg'), _('Exit the program'), CommandType.METHOD, 'sys.exit', '0'),
        ]),
        CommandCategory(p, text('📜'), [
            CommandDefinition('0', text('→'), _('East (0°)'), CommandType.LITERAL_CONST, '0'),
            CommandDefinition('90', text('↑'), _('North (90°)'), CommandType.LITERAL_CONST, '90'),
            CommandDefinition('180', text('←'), _('West (180°)'), CommandType.LITERAL_CONST, '180'),
            CommandDefinition('270', text('↓'), _('South (270°)'), CommandType.LITERAL_CONST, '270'),
            CommandDefinition('math.pi', text('π'), _('Pi (3.14)'), CommandType.LITERAL_CONST, 'math.pi'),
            CommandDefinition('sin', text('sin'), _('Sine'), CommandType.METHOD, 'math.sin'),
            CommandDefinition('cos', text('cos'), _('Cosine'), CommandType.METHOD, 'math.cos'),
            CommandDefinition('tan', text('tan'), _('Tangent'), CommandType.METHOD, 'math.tan'),
            CommandDefinition('abs', text('abs'), _('Absolute value'), CommandType.METHOD, 'abs'),
            CommandDefinition('rad', text('rad'), _('Degrees to radians'), CommandType.METHOD, 'math.radians'),
            CommandDefinition('deg', text('deg'), _('Radians to degrees'), CommandType.METHOD, 'math.degrees'),
            CommandDefinition('true', icon('true.svg'), _('True'), CommandType.LITERAL_CONST, 'True', snippet='false;'),
            CommandDefinition('false', icon('false.svg'), _('False'), CommandType.LITERAL_CONST, 'False', snippet='true;'),
            CommandDefinition('rnd', text('rnd'), _('Round a number'), CommandType.METHOD, 'round'),
            CommandDefinition('floor', text('flr'), _('Floor'), CommandType.METHOD, 'math.floor'),
            CommandDefinition('ceil', text('ceil'), _('Ceil'), CommandType.METHOD, 'math.ceil'),
            CommandDefinition('sqrt', text('sqrt'), _('Square root'), CommandType.METHOD, 'math.sqrt'),
            CommandDefinition('mmin', text('min'), _('Lowest value'), CommandType.METHOD, 'min'),
            CommandDefinition('mmax', text('max'), _('Highest value'), CommandType.METHOD, 'max'),
            CommandDefinition('color', icon('color.svg'), _('Color (property or editable)'), CommandType.LITERAL, color, data_only=True),
            CommandDefinition('font', icon('font.svg'), _('Font (property or editable)'), CommandType.LITERAL, font),
            CommandDefinition('int', text('🔢'), _('Number'), CommandType.LITERAL, raw_data, data_only=True, color=CommandColor.NUMBER),
            CommandDefinition('str', text('🔤'), _('String'), CommandType.LITERAL, string, data_only=True, color=CommandColor.STRING),
            CommandDefinition('obj', text('🆔'), _('Object'), CommandType.LITERAL, raw_data, data_only=True, color=CommandColor.OBJECT),
            CommandDefinition('none', icon('none.svg'), _('None'), CommandType.LITERAL_CONST, 'None'),
            CommandDefinition('key', icon('key.svg'), _('Key'), CommandType.LITERAL, string, data_only=False),
        ]),
        CommandCategory(p, text('💾'), [
            CommandDefinition('readlines', text('📤'), _('Read all lines from file'), CommandType.METHOD, 'tcf_readlines'),
            CommandDefinition('writelines', text('📥'), _('Write lines to a file'), CommandType.METHOD, 'tcf_writelines'),
            CommandDefinition('filediag', icon('open_file.svg'), _('File dialog'), CommandType.METHOD, 'tcf_filedialog'),
            CommandDefinition('fileis', icon('fileis.svg'), _('Check if file exists'), CommandType.METHOD, 'os.path.isfile'),
            CommandDefinition('diris', icon('diris.svg'), _('Check if directory exists'), CommandType.METHOD, 'os.path.isdir'),
            CommandDefinition('filedirdel', text('␡'), _('Delete file or directory'), CommandType.METHOD, 'tcf_filedirdel'),
            CommandDefinition('subfiles', icon('subfiles.svg'), _('Get files in directory'), CommandType.METHOD, 'tcf_subfiles'),
            CommandDefinition('subdirs', icon('subdirs.svg'), _('Get subdirs in directory'), CommandType.METHOD, 'tcf_subdirs'),
            CommandDefinition('dirname', icon('dirname.svg'), _('Get parent directory'), CommandType.METHOD, 'os.path.dirname'),
            CommandDefinition('basename', icon('basename.svg'), _('Get file name'), CommandType.METHOD, 'os.path.basename'),
            CommandDefinition('runc', icon('runc.svg'), _('Run system command'), CommandType.METHOD, 'os.system'),
            CommandDefinition('runf', icon('runf.svg'), _('Open file in default program'), CommandType.METHOD, 'tcf_runf'),
            CommandDefinition('time', icon('time.svg'), _('Get current timestamp'), CommandType.METHOD, 'time.time'),
            CommandDefinition('timestrf', icon('time_strf.svg'), _('Convert timestamp to string'), CommandType.METHOD, 'tcf_strftime'),
            CommandDefinition('timestrp', icon('time_strp.svg'), _('Convert string to timestamp'), CommandType.METHOD, 'tcf_strptime'),
        ]),
    ]
    p.modules = {
        'base': CommandModule(
            deps=(),
            code="""from tempfile import NamedTemporaryFile
import math, random, os, time, sys
from datetime import datetime
os.chdir(os.path.dirname(os.path.abspath(__file__)))"""
        ),
        'tcf_readlines': CommandModule(
            deps=(),
            code="""def tcf_readlines(file=None):
	if file == None: file = __file__ + '.save'
	f = open(file, 'r'); ret = f.readlines(); f.close()
	ret = [x.replace('\\n', '') for x in ret]
	return ret"""
        ),
        'tcf_writelines': CommandModule(
            deps=(),
            code="""def tcf_writelines(data, file=None):
	if file == None: file = __file__ + '.save'
	data = [x + '\\n' for x in data]
	f = open(file, 'w'); f.writelines(data); f.close()"""
        ),
        'tcf_filedialog': CommandModule(
            deps=(),
            code="""def tcf_filedialog(text = 'Choose a file', filter = 'All files | *', save = False):
	if sys.platform.startswith('linux'):
		from subprocess import Popen, PIPE, DEVNULL
		my_env=os.environ.copy()
		my_env['G_MESSAGES_DEBUG']=''
		if save: save = '--save'
		else: save = ''
		proc = Popen(
			['zenity', '--file-selection', '--title', text,
			'--file-filter', filter, save], stdout=PIPE, stderr=DEVNULL, stdin=DEVNULL, env=my_env)
		(output, err) = proc.communicate(); proc.wait()
		output = output.decode().replace('\\n', ''); return output
	else:
		f = filter.split(' | ')
		if len(f) == 2:
			filetypes = ((f[0], f[1]),)
		elif len(f) == 1:
			filetypes = (('Files', f[0]),)
		from tkinter import filedialog
		if save:
			path = filedialog.asksaveasfilename(title=text, filetypes=filetypes)
			return path
		else:
			path = filedialog.askopenfile(title=text, filetypes=filetypes)
			return None if path is None else path.name"""
        ),
        'tcf_filedirdel': CommandModule(
            deps=(),
            code="""def tcf_filedirdel(path):
	if os.path.isfile(path):
		os.remove(path)
	elif os.path.isdir(path):
		import shutil
		shutil.rmtree(path)
	else: raise Exception('File or directory {} does not exist!'.format(path))"""
        ),
        'tcf_runf': CommandModule(
            deps=(),
            code="""def tcf_runf(file):
	import subprocess
	if sys.platform.startswith('linux'):
		from gi.repository import Gio
		result = 1
		file = Gio.File.new_for_commandline_arg(file)
		result = int(not Gio.AppInfo.launch_default_for_uri(file.get_uri(), None))
	else:
		result = subprocess.run(['start', '', file], shell=True).returncode
	if result != 0:
		raise Exception('\\nFile opening failed.')"""
        ),
        'tcf_subfiles': CommandModule(
            deps=(),
            code="""def tcf_subfiles(path):
	result = []
	for entry in os.scandir(path):
		if entry.is_file():
			result.append(entry.path)
	result.sort()
	return result"""
        ),
        'tcf_subdirs': CommandModule(
            deps=(),
            code="""def tcf_subdirs(path):
	result = []
	for entry in os.scandir(path):
		if entry.is_dir():
			result.append(entry.path)
	result.sort()
	return result"""
        ),
        'tcf_index': CommandModule(
            deps=(),
            code="""def tcf_index(list, item, start=0, direction=1):
	if direction == 0: raise Exception('Direction can not be zero.')
	if direction < 0: end = 0
	if direction > 0: end = len(list)
	for i in range(start, end, direction):
		if list[i] == item:
			return i
	return -1"""
        ),
        'tcf_mdlist': CommandModule(
            deps=(),
            code="""def tcf_mdlist(default_value, *size):
	if len(size) < 1:
		raise Exception('The list must have at least one dimension.')
	if len(size) == 1:
		return [default_value] * size[0]
	return [tcf_mdlist(default_value, *size[1:]) for i in range(size[0])]"""
        ),
        'tcf_strftime': CommandModule(
            deps=(),
            code="""def tcf_strftime(time, format='%d/%m/%y %H:%M:%S'):
	return datetime.fromtimestamp(time).strftime(format)"""
        ),
        'tcf_strptime': CommandModule(
            deps=(),
            code="""def tcf_strptime(string_time, format='%d/%m/%y %H:%M:%S'):
	return datetime.timestamp(datetime.strptime(string_time, format))"""
        ),
    }

    return p
