# Copyright (C) 2021 saytamkenorh
#
# This file is part of Turtlico.
#
# Turtlico is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turtlico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Turtlico.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

import os

from gi.repository import Gtk

import turtlico.app as app


@Gtk.Template(resource_path='/io/gitlab/Turtlico/ui/appsettings.ui')
class AppSettingsWindow(Gtk.Dialog):
    __gtype_name__ = 'TurtlicoAppSettingsWindow'

    _auto_ident_switch = Gtk.Template.Child()
    _code_preview_switch = Gtk.Template.Child()
    _csd_switch = Gtk.Template.Child()
    _dark_icons_switch = Gtk.Template.Child()
    _dark_mode_switch = Gtk.Template.Child()

    def __init__(self, application: app.Application, parent: Gtk.Window):
        super().__init__(
            transient_for=parent,
            modal=True,
        )
        self._auto_ident_switch.props.key_settings = application.settings
        self._code_preview_switch.props.key_settings = application.settings
        self._csd_switch.props.key_settings = application.settings
        self._csd_switch.props.parent.props.visible = (
            os.environ.get('GTK_CSD', '1') != '0')
        self._dark_icons_switch.props.key_settings = application.settings
        self._dark_mode_switch.props.key_settings = application.settings
