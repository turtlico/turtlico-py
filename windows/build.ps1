$oldcd = Get-Location
Set-Location $PSScriptRoot

choco install msys2 -y

filter replace-slash {$_ -replace "\\", "/"}
C:\tools\msys64\usr\bin\bash.exe -lc "$(Get-Location | replace-slash)/build.sh"
if ($LASTEXITCODE -ne 0) {
    Write-Host "Turtlico build script failed"
    exit $LASTEXITCODE
}

choco install innosetup -y
iscc .\build\turtlico.iss /Q /O$(Get-Location) /Fturtlico-setup

Set-Location $oldcd