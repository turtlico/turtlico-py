# Copyright (C) 2021 saytamkenorh
#
# This file is part of Turtlico.
#
# Turtlico is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turtlico is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Turtlico.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from typing import Union
import urllib.parse

from gi.repository import GObject, Gtk, Gdk, Gio

import turtlico.app as app
import turtlico.app.widgets as widgets
import turtlico.lib as lib
import turtlico.utils as utils


@Gtk.Template(resource_path='/io/gitlab/Turtlico/ui/programviewmenu.ui')
class ProgramViewMenu(Gtk.PopoverMenu):
    __gtype_name__ = 'TurtlicoProgramViewMenu'

    programview = GObject.Property(type=widgets.ProgramView)
    opened_icon: Union[tuple[int, int], None]  # The last opened icon coords

    _edit_action: Gio.SimpleAction
    _paste_action: Gio.SimpleAction
    _help_action: Gio.SimpleAction
    _autocomplete_action: Gio.SimpleAction

    def __init__(self):
        super().__init__()
        self.opened_icon = None

        action_group = Gio.SimpleActionGroup.new()

        self._edit_action = utils.group_add_action(
            action_group, 'edit-icon', self._on_edit)
        self._paste_action = utils.group_add_action(
            action_group, 'edit-paste', self._on_paste)
        self._help_action = utils.group_add_action(
            action_group, 'icon-help', self._on_help)
        self._autocomplete_action = utils.group_add_action(
            action_group, 'autocomplete', self._on_autocomplete)

        self.insert_action_group('menu', action_group)

        shortcut_controller = Gtk.ShortcutController()
        shortcut_controller.add_shortcut(
            utils.new_action_shortcut('F2', 'menu.edit-icon'))
        shortcut_controller.add_shortcut(
            utils.new_action_shortcut('<Ctrl>V', 'menu.edit-paste'))
        self.add_controller(shortcut_controller)

    def open_at_icon(self, x: int, y: int):
        cmd, cx, cy = self.props.programview.get_command_at(x, y)
        self.props.has_arrow = cmd is not None

        if cmd is not None:
            self.opened_icon = (cx, cy)
            self._edit_action.props.enabled = widgets.get_icon_editable(cmd)

            # Shows the context menu in the center of the icon
            spx, spy = self.props.programview.get_coords_at(cx, cy, False)
            epx, epy = self.props.programview.get_coords_at(cx, cy, True)
            spx = min(
                max(spx, 0),
                self.props.programview.get_width() - lib.icon.WIDTH)
            epx = min(
                max(epx, 0),
                self.props.programview.get_width() - lib.icon.WIDTH)

            rect = Gdk.Rectangle()
            rect.x = spx
            rect.y = spy
            rect.width = epx - spx + lib.icon.WIDTH
            rect.height = lib.icon.HEIGHT
            self.props.pointing_to = rect

            selection = self.props.programview.selection
            if selection is not None and not selection.contains(cx, cy):
                self.props.programview.selection = None
        else:
            self.opened_icon = None
            self._edit_action.props.enabled = False

            rect = Gdk.Rectangle()
            rect.x = x
            rect.y = y
            rect.width = 1
            rect.height = 1
            self.props.pointing_to = rect

            self.props.programview.selection = None

        self._paste_action.props.enabled = (
            lib.get_tcppiece_from_clipboard(self) is not None)
        self._help_action.props.enabled = (
            self.opened_icon is not None
            and self._get_doc_url(cmd) is not None)
        self._autocomplete_action.props.enabled = (
            self.opened_icon is not None
            and cmd.definition.snippet is not None
        )

        self.popup()

    def _on_edit(self, action, args):
        self.popdown()
        x, y = self.opened_icon
        self.props.programview.edit_command(x, y)

    def _on_paste(self, action, args):
        self.popdown()

        buf = self.props.programview.get_codebuffer()

        tcppiece = lib.get_tcppiece_from_clipboard(self)
        commands = lib.load_codepiece(tcppiece, buf.project)

        if self.opened_icon is not None:
            cx, cy = self.opened_icon
        else:
            x = self.props.pointing_to.x
            y = self.props.pointing_to.y
            cx, cy = self.props.programview.drop_coords_to_program(x, y)

        buf.insert(commands, cx, cy, self.props.programview.props.auto_indent)

    def _on_help(self, action, args):
        self.popdown()
        x, y = self.opened_icon
        cmd: lib.Command = self.props.programview.get_codebuffer().lines[y][x]
        fragment = lib.CommandDefinition.escape_id_for_html(cmd.definition.id)

        doc_url = self._get_doc_url(cmd)
        if doc_url is None:
            return

        Gtk.show_uri(
            utils.get_parent_window(self),
            urllib.parse.urljoin(
                doc_url,
                f'#{fragment}'),
            Gdk.CURRENT_TIME)

    def _on_autocomplete(self, action, args):
        self.popdown()
        x, y = self.opened_icon

        cbuf = self.props.programview.get_codebuffer()
        cmd: lib.Command = cbuf.lines[y][x]

        snippet = lib.load_codepiece(
            cmd.definition.snippet, cbuf.project)

        self.props.programview.selection = lib.CodePieceSelection(
            x, y, x, y
        )
        self.props.programview.replace_selection(
            snippet
        )

    def _get_doc_url(self, command: lib.Command) -> str:
        buffer: lib.CodeBuffer = self.props.programview.get_codebuffer()
        project = buffer.project
        plugin = project.get_definition_plugin(command.definition)
        if plugin.doc_url is None:
            return None
        return urllib.parse.urljoin(
            app.Application.get_doc_uri(), plugin.doc_url)
